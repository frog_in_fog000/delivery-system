# build
FROM golang:1.22.1-alpine AS builder

WORKDIR /app

RUN apk --no-cache add bash git make

COPY ["model-receiver/go.mod", "model-receiver/go.sum", "./"]

RUN go mod download

COPY model-receiver .

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/app cmd/*.go

# run
FROM alpine as runner

COPY --from=builder /app/bin/app /
COPY model-receiver/.env /config.env

EXPOSE 8082

CMD ["/app"]