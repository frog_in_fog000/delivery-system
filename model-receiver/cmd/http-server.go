package main

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/handlers"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/service"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/storage"
)

type Handlers struct {
	ReceiverHandler handlers.ReceiverHandler
}

func InitHttpHandlers(cfg *config.Config, dispatcherStorage storage.ReceiverStorage, log logger.Logger) *Handlers {
	// service
	dispatcherService := service.NewDispatcherService(dispatcherStorage, &log)

	// handlers
	dispatcherHandler := handlers.NewDispatcherHandler(dispatcherService, &log, cfg)

	return &Handlers{ReceiverHandler: dispatcherHandler}
}
