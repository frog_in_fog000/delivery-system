package main

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

func (h *Handlers) InitRoutes() http.Handler {
	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler())

	router.HandleFunc("/receiver", h.ReceiverHandler.SaveData)

	return router
}
