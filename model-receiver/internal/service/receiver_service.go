package service

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/storage"
)

type ReceiverService interface {
	SaveStaticData(data *models.GJSStaticOutput) error
	SaveDynamicData(data *models.GJSDynamicOutput) error
	StartRabbitMQListener(log *logger.Logger)
}

type receiverService struct {
	receiverStorage storage.ReceiverStorage
	log             *logger.Logger
}

func NewDispatcherService(receiverStorage storage.ReceiverStorage, log *logger.Logger) ReceiverService {
	return &receiverService{receiverStorage: receiverStorage, log: log}
}
