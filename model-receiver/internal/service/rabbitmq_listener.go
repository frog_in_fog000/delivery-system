package service

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/storage/redis"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"
	"go.uber.org/zap"
)

const (
	AmqpServerUrl = "amqp://guest:guest@rabbitmq:5672/"
)

func (s *receiverService) StartRabbitMQListener(log *logger.Logger) {
	connectRabbitMQ, err := amqp.DialConfig(AmqpServerUrl, amqp.Config{
		Heartbeat: 3 * time.Second,
	})
	if err != nil {
		log.Logger.Warn("trying to reconnect to rabbitmq", zap.Error(err))
	}
	defer connectRabbitMQ.Close()

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		log.Logger.Warn("trying to reconnect to rabbitmq", zap.Error(err))
	}
	defer channelRabbitMQ.Close()

	messages, err := channelRabbitMQ.Consume(
		"GeoJSONOutput", // queue name
		"",              // consumer
		true,            // auto-ack
		false,           // exclusive
		false,           // no local
		false,           // no wait
		nil,             // arguments
	)
	if err != nil {
		log.Logger.Error("Failed to register a consumer")
		return
	}

	forever := make(chan bool)
	go func() {
		for message := range messages {
			log.Logger.Info(fmt.Sprintf(" > Received message: %s\n", string(message.Body)))

			var staticItem models.GJSStaticOutput
			var dynamicItem models.GJSDynamicOutput

			typeOfData := message.Headers["type"].(string)

			if typeOfData == "static" {
				if err = json.Unmarshal(message.Body, &staticItem); err != nil {
					log.Logger.Error("failed to unmarshal static item", zap.Error(err))
					return
				}

				staticItemJson, err := json.Marshal(staticItem)
				if err != nil {
					log.Logger.Error("Failed to marshal json", zap.Error(err))
					return
				}
				err = redis.RedisClient.Set(context.Background(), fmt.Sprintf("%s:%s:%s", staticItem.RequestData.UserId, staticItem.RequestData.RequestId, "static"), staticItemJson, 1*time.Minute).Err()
				if err != nil {
					log.Logger.Error("Failed to save data", zap.Error(err))
					return
				}

				if err := s.SaveStaticData(&staticItem); err != nil {
					log.Logger.Error("Failed to save static data", zap.Error(err))
					return
				}
				log.Logger.Info(fmt.Sprintf("Static data: %s saved in database\n", string(message.Body)))
			} else if typeOfData == "dynamic" {
				if err = json.Unmarshal(message.Body, &dynamicItem); err != nil {
					log.Logger.Error("failed to unmarshal dynamic item", zap.Error(err))
				}

				dynamicItemJson, err := json.Marshal(dynamicItem)
				if err != nil {
					log.Logger.Error("Failed to marshal json", zap.Error(err))
					return
				}
				err = redis.RedisClient.Set(context.Background(), fmt.Sprintf("%s:%s:%s", dynamicItem.RequestData.UserId, dynamicItem.RequestData.RequestId, "dynamic"), dynamicItemJson, 1*time.Minute).Err()
				if err != nil {
					log.Logger.Error("Failed to save data", zap.Error(err))
					return
				}

				if err := s.SaveDynamicData(&dynamicItem); err != nil {
					log.Logger.Error("Failed to save data", zap.Error(err))
					return
				}
				log.Logger.Info(fmt.Sprintf("Dynamic data: %s saved in database\n", string(message.Body)))
			} else {
				log.Logger.Error("Unknown message type", zap.String("type", typeOfData))
				return
			}
		}
	}()
	<-forever
}
