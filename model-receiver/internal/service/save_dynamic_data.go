package service

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"
	"go.uber.org/zap"
)

func (s *receiverService) SaveDynamicData(data *models.GJSDynamicOutput) error {
	if err := s.receiverStorage.SaveDynamicData(data); err != nil {
		s.log.Logger.Error("save data error", zap.Error(err))
		return err
	}
	return nil
}
