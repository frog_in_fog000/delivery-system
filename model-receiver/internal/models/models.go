package models

import "time"

type ModelData struct {
	Name string `json:"name" bson:"name"`
	Data string `json:"data" bson:"data"`
}

type Response struct {
	VisualizationURL string `json:"visualization_url" bson:"visualization_url"`
	DownloadCSVURL   string `json:"download_csv_url" bson:"download_csv_url"`
	SendEmailCSVURL  string `json:"send_email_csv_url" bson:"send_email_csv_url"`
}

type GJSDynamicOutput struct {
	RequestData struct {
		UserId    string    `json:"user_id" bson:"user_id"`
		RequestId string    `json:"request_id" bson:"request_id"`
		CreatedAt time.Time `json:"created_at" bson:"created_at"`
		ModelType string    `json:"model_type" bson:"model_type"`
	} `json:"request_data" bson:"request_data"`
	DynamicModel struct {
		Features []struct {
			Geometry struct {
				Coordinates [][]float64 `json:"coordinates" bson:"coordinates"`
				Type        string      `json:"type" bson:"type"`
			} `json:"geometry" bson:"geometry"`
			Type       string `json:"type" bson:"type"`
			Properties struct {
				Color string `json:"color" bson:"color"`
				Type  string `json:"type" bson:"type"`
			} `json:"properties" bson:"properties"`
		} `json:"features" bson:"features"`
		Type string `json:"type" bson:"type"`
	} `json:"dynamic_model" bson:"dynamic_model"`
	DynamicModelParameters struct {
		TotalTime struct {
			Hours   int `json:"hours" bson:"hours"`
			Minutes int `json:"minutes" bson:"minutes"`
			Seconds int `json:"seconds" bson:"seconds"`
		} `json:"total_time" bson:"total_time"`
		TotalExpenses        float64 `json:"total_expenses" bson:"total_expenses"`
		OptimalCouriersCount int     `json:"optimal_couriers_count" bson:"optimal_couriers_count"`
	} `json:"dynamic_model_parameters" bson:"dynamic_model_parameters"`
}

type GJSStaticOutput struct {
	RequestData struct {
		UserId    string    `json:"user_id" bson:"user_id"`
		RequestId string    `json:"request_id" bson:"request_id"`
		CreatedAt time.Time `json:"created_at" bson:"created_at"`
		ModelType string    `json:"model_type" bson:"model_type"`
	} `json:"request_data" bson:"request_data"`
	StaticModel struct {
		Features []struct {
			Geometry struct {
				Coordinates [][]float64 `json:"coordinates"`
				Type        string      `json:"type"`
			} `json:"geometry"`
			Type       string `json:"type"`
			Properties struct {
				Color string `json:"color"`
				Type  string `json:"type"`
			} `json:"properties"`
		} `json:"features"`
		Type string `json:"type"`
	} `json:"static_model"`
	StaticModelParameters struct {
		CouriersCount int `json:"couriers_count"`
		TotalTime     struct {
			Hours   int `json:"hours"`
			Minutes int `json:"minutes"`
			Seconds int `json:"seconds"`
		} `json:"total_time"`
		TotalExpenses float64 `json:"total_expenses"`
	} `json:"static_model_parameters"`
}

//type OutputData struct {
//	UserId                 string    `json:"user_id" bson:"user_id"`
//	RequestId              string    `json:"request_id" bson:"request_id"`
//	ModelType              string    `json:"model_type" bson:"model_type"`
//	CreatedAt              time.Time `json:"created_at" bson:"created_at"`
//	CalculatedStaticValues struct {
//		StaticFuelCost  string `json:"static_fuel_cost" bson:"static_fuel_cost"`
//		StaticTimeSpent string `json:"static_time_spent" bson:"static_time_spent"`
//	} `json:"calculated_static_values" bson:"calculated_static_values"`
//	CalculatedDynamicValues struct {
//		DynamicFuelCost  string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
//		DynamicTimeSpent string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
//	} `json:"calculated_dynamic_values" bson:"calculated_dynamic_values"`
//	Data struct {
//		Type     string    `json:"type" bson:"type"`
//		Features []Feature `json:"features" bson:"features"`
//	} `json:"data" bson:"data"`
//}

//type OutputData struct {
//	UserId                 string `json:"user_id" bson:"user_id"`
//	RequestId              string `json:"request_id" bson:"request_id"`
//	ModelType              string `json:"model_type" bson:"model_type"`
//	CalculatedStaticValues struct {
//		StaticFuelCost  string `json:"static_fuel_cost" bson:"static_fuel_cost"`
//		StaticTimeSpent string `json:"static_time_spent" bson:"static_time_spent"`
//	} `json:"calculated_static_values" bson:"calculated_static_values"`
//	CalculatedDynamicValues struct {
//		DynamicFuelCost  string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
//		DynamicTimeSpent string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
//	} `json:"calculated_dynamic_values" bson:"calculated_dynamic_values"`
//	Data struct {
//		Type     string `json:"type" bson:"type"`
//		Features []struct {
//			Type     string `json:"type" bson:"type"`
//			Geometry struct {
//				Type        string    `json:"type" bson:"type"`
//				Coordinates [][][]int `json:"coordinates" bson:"coordinates"`
//			} `json:"geometry" bson:"geometry"`
//		} `json:"features" bson:"features"`
//	} `json:"data" bson:"data"`
//}
