package config

import (
	"github.com/spf13/viper"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"go.uber.org/zap"
)

type Config struct {
	WebPort   string `mapstructure:"WEB_PORT"`
	RedisURL  string `mapstructure:"REDIS_URL"`
	MongoURL  string `mapstructure:"MONGO_URL"`
	MongoName string `mapstructure:"MONGODB_NAME"`
	MongoUser string `mapstructure:"MONGO_USER"`
	MongoPass string `mapstructure:"MONGO_PASSWORD"`
}

func LoadConfig(path string, log *logger.Logger) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		log.Logger.Error("Error reading config file", zap.Error(err))
		return
	}

	if err = viper.Unmarshal(&config); err != nil {
		log.Logger.Error("Error unmarshalling config", zap.Error(err))
		return
	}
	return
}
