package redis

import (
	"context"
	"github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"go.uber.org/zap"
)

var (
	RedisClient *redis.Client
	ctx         context.Context
)

func NewRedisConnection(config *config.Config, log logger.Logger) error {
	ctx = context.Background()

	RedisClient = redis.NewClient(&redis.Options{
		Addr:     config.RedisURL,
		Password: "",
		DB:       0,
	})

	if _, err := RedisClient.Ping(ctx).Result(); err != nil {
		log.Logger.Error("output data redis connection error", zap.Error(err))
		return err
	}

	log.Logger.Info("output data redis client connected successfully")
	return nil
}
