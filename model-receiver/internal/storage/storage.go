package storage

import "gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"

type ReceiverStorage interface {
	SaveStaticData(data *models.GJSStaticOutput) error
	SaveDynamicData(data *models.GJSDynamicOutput) error
}
