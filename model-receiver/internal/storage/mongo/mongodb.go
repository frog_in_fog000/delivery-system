package mongo

import (
	"context"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/storage"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type mongoDB struct {
	mongoClient *mongo.Client
	log         *logger.Logger
}

func NewMongoDB(cfg *config.Config, log *logger.Logger) (storage.ReceiverStorage, error) {
	clientOpts := options.Client().ApplyURI(cfg.MongoURL)

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		log.Logger.Error("error connecting to mongo", zap.Error(err))
		return nil, err
	}

	return &mongoDB{
		mongoClient: client,
		log:         log,
	}, nil
}

func (m *mongoDB) SaveStaticData(data *models.GJSStaticOutput) error {
	collection := m.mongoClient.Database("dispatcher").Collection("model_results")

	_, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		m.log.Logger.Error("error inserting static data", zap.Error(err))
		return err
	}
	return nil
}

func (m *mongoDB) SaveDynamicData(data *models.GJSDynamicOutput) error {
	collection := m.mongoClient.Database("dispatcher").Collection("model_results")

	_, err := collection.InsertOne(context.TODO(), data)
	if err != nil {
		m.log.Logger.Error("error inserting dynamic data", zap.Error(err))
		return err
	}
	return nil
}
