package handlers

import (
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/service"
)

type ReceiverHandler interface {
	SaveData(w http.ResponseWriter, r *http.Request)
}

type receiverHandler struct {
	receiverService service.ReceiverService
	log             *logger.Logger
	cfg             *config.Config
}

func NewDispatcherHandler(receiverService service.ReceiverService, log *logger.Logger, cfg *config.Config) ReceiverHandler {
	return &receiverHandler{receiverService: receiverService, log: log, cfg: cfg}
}
