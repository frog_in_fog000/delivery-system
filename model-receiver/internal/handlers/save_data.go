package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/model-receiver/internal/storage/redis"
	"go.uber.org/zap"
	"net/http"
	"time"
)

func (h *receiverHandler) SaveData(w http.ResponseWriter, r *http.Request) {
	go h.receiverService.StartRabbitMQListener(h.log)
	userId := r.URL.Query().Get("user_id")
	requestId := r.URL.Query().Get("request_id")

	staticKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "static")
	dynamicKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "dynamic")

	// Параметры повторных попыток
	maxRetries := 5
	retryInterval := 1 * time.Second

	var outputStaticData models.GJSStaticOutput
	var outputDynamicData models.GJSDynamicOutput

	for retries := 0; retries < maxRetries; retries++ {
		// Создание транзакции
		tx := redis.RedisClient.TxPipeline()

		// Получаем данные по первому ключу в транзакции
		static := tx.Get(context.Background(), staticKey)

		// Получаем данные по второму ключу в транзакции
		dynamic := tx.Get(context.Background(), dynamicKey)

		// Выполнение транзакции
		_, err := tx.Exec(context.Background())
		if err == nil {
			// Получение результатов транзакции
			staticBytes, err := static.Bytes()
			if err != nil {
				h.log.Logger.Error("error receiving data by static key", zap.Error(err))
				return
			}

			if err := json.Unmarshal(staticBytes, &outputStaticData); err != nil {
				h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
				return
			}

			dynamicBytes, err := dynamic.Bytes()
			if err != nil {
				h.log.Logger.Error("error receiving data by dynamic key 2", zap.Error(err))
				return
			}

			if err := json.Unmarshal(dynamicBytes, &outputDynamicData); err != nil {
				h.log.Logger.Error("error unmarshalling data by static key 2", zap.Error(err))
				return
			}

			break // Если данные получены успешно, выходим из цикла
		} else {
			h.log.Logger.Warn(fmt.Sprintf("error making transacttion: %v. Retry in %v", err, retryInterval))
			time.Sleep(retryInterval)
		}
	}

	var response models.Response
	response.VisualizationURL = fmt.Sprintf("http://localhost:8080/visualizer/show/%s", requestId)
	response.DownloadCSVURL = fmt.Sprintf("http://localhost:8080/visualizer/save/%s", requestId)
	response.SendEmailCSVURL = fmt.Sprintf("http://localhost:8080/visualizer/email/%s", requestId)

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		h.log.Logger.Error("error marshalling data", zap.Error(err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)

}
