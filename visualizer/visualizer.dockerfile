# build
FROM golang:1.22.1-alpine AS builder

WORKDIR /app

RUN apk --no-cache add bash git make

COPY ["visualizer/go.mod", "visualizer/go.sum", "./"]

RUN go mod download

COPY visualizer .

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/app cmd/*.go

# run
FROM alpine as runner

COPY --from=builder /app/bin/app /
COPY visualizer/.env /config.env

EXPOSE 8083

CMD ["/app"]