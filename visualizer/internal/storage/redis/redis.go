package redis

import (
	"context"
	"github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/logger"
	"go.uber.org/zap"
)

var (
	RedisClient *redis.Client
	ctx         context.Context
)

func NewRedisConnection(config *config.Config, log logger.Logger) error {
	ctx = context.Background()

	RedisClient = redis.NewClient(&redis.Options{
		Addr:     config.RedisURL,
		Password: "",
		DB:       0,
	})

	if _, err := RedisClient.Ping(ctx).Result(); err != nil {
		log.Logger.Error("visualizer redis connection error", zap.Error(err))
		return err
	}

	log.Logger.Info("visualizer redis client connected successfully")
	return nil
}
