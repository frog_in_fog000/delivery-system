package mongo

import (
	"context"
	"errors"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (m *mongoDB) GetDynamicDataByRequestID(requestId string, modelType string) (*models.GJSDynamicOutput, error) {
	var result models.GJSDynamicOutput

	collection := m.mongoClient.Database("dispatcher").Collection("model_results")

	filter := bson.M{
		"request_data.request_id": requestId,
		"request_data.model_type": modelType,
	}

	if err := collection.FindOne(context.TODO(), filter).Decode(&result); err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return &models.GJSDynamicOutput{}, errors.New("record not found")
		}

		return nil, err
	}

	return &result, nil
}
