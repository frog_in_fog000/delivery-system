package mongo

import (
	"context"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.uber.org/zap"
	"time"
)

func (m *mongoDB) GetAllRequestIDsByUserId(id string) ([]models.RequestIDWithTimestamp, error) {
	var result []models.RequestIDWithTimestamp
	collection := m.mongoClient.Database("dispatcher").Collection("model_results")

	pipeline := bson.A{
		bson.D{{"$match", bson.D{{"request_data.user_id", id}}}},
		bson.D{{"$sort", bson.D{{"request_data.created_at", -1}}}}, // Сортируем по убыванию created_at
		bson.D{{"$group", bson.D{
			{"_id", "$request_data.request_id"},
			{"request_id", bson.D{{"$first", "$request_data.request_id"}}},
			{"created_at", bson.D{{"$first", "$request_data.created_at"}}},
		}}},
	}

	cursor, err := collection.Aggregate(context.TODO(), pipeline)
	if err != nil {
		m.log.Logger.Error("error getting data", zap.Error(err))
		return nil, err
	}

	var tempResult []struct {
		RequestID string    `bson:"request_id"`
		CreatedAt time.Time `bson:"created_at"`
	}
	if err = cursor.All(context.TODO(), &tempResult); err != nil {
		m.log.Logger.Error("error decoding data", zap.Error(err))
		return nil, err
	}

	for _, item := range tempResult {
		result = append(result, models.RequestIDWithTimestamp{
			RequestID: item.RequestID,
			CreatedAt: item.CreatedAt,
		})
	}

	return result, nil
}
