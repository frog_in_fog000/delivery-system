package mongo

import (
	"context"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type mongoDB struct {
	mongoClient *mongo.Client
	log         *logger.Logger
}

func NewMongoDB(cfg *config.Config, log *logger.Logger) (storage.VisualizerStorage, error) {
	clientOpts := options.Client().ApplyURI(cfg.MongoURL)

	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		log.Logger.Error("error connecting to mongo", zap.Error(err))
		return nil, err
	}

	return &mongoDB{
		mongoClient: client,
		log:         log,
	}, nil
}
