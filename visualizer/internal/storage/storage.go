package storage

import "gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"

type VisualizerStorage interface {
	GetAllRequestIDsByUserId(id string) ([]models.RequestIDWithTimestamp, error)
	GetStaticDataByRequestID(requestId string, modelType string) (*models.GJSStaticOutput, error)
	GetDynamicDataByRequestID(requestId string, modelType string) (*models.GJSDynamicOutput, error)
}
