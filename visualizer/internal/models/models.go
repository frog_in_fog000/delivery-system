package models

import "time"

type GJSDynamicOutput struct {
	RequestData struct {
		UserId    string    `json:"user_id" bson:"user_id"`
		RequestId string    `json:"request_id" bson:"request_id"`
		CreatedAt time.Time `json:"created_at" bson:"created_at"`
		ModelType string    `json:"model_type" bson:"model_type"`
	} `json:"request_data" bson:"request_data"`
	DynamicModel struct {
		Features []struct {
			Geometry struct {
				Coordinates [][]float64 `json:"coordinates" bson:"coordinates"`
				Type        string      `json:"type" bson:"type"`
			} `json:"geometry" bson:"geometry"`
			Type       string `json:"type" bson:"type"`
			Properties struct {
				Color string `json:"color" bson:"color"`
				Type  string `json:"type" bson:"type"`
			} `json:"properties" bson:"properties"`
		} `json:"features" bson:"features"`
		Type string `json:"type" bson:"type"`
	} `json:"dynamic_model" bson:"dynamic_model"`
	DynamicModelParameters struct {
		TotalTime struct {
			Hours   int `json:"hours" bson:"hours"`
			Minutes int `json:"minutes" bson:"minutes"`
			Seconds int `json:"seconds" bson:"seconds"`
		} `json:"total_time" bson:"total_time"`
		TotalExpenses        float64 `json:"total_expenses" bson:"total_expenses"`
		OptimalCouriersCount int     `json:"optimal_couriers_count" bson:"optimal_couriers_count"`
	} `json:"dynamic_model_parameters" bson:"dynamic_model_parameters"`
}

type GJSStaticOutput struct {
	RequestData struct {
		UserId    string    `json:"user_id" bson:"user_id"`
		RequestId string    `json:"request_id" bson:"request_id"`
		CreatedAt time.Time `json:"created_at" bson:"created_at"`
		ModelType string    `json:"model_type" bson:"model_type"`
	} `json:"request_data" bson:"request_data"`
	StaticModel struct {
		Features []struct {
			Geometry struct {
				Coordinates [][]float64 `json:"coordinates"`
				Type        string      `json:"type"`
			} `json:"geometry"`
			Type       string `json:"type"`
			Properties struct {
				Color string `json:"color"`
				Type  string `json:"type"`
			} `json:"properties"`
		} `json:"features"`
		Type string `json:"type"`
	} `json:"static_model"`
	StaticModelParameters struct {
		CouriersCount int `json:"couriers_count"`
		TotalTime     struct {
			Hours   int `json:"hours"`
			Minutes int `json:"minutes"`
			Seconds int `json:"seconds"`
		} `json:"total_time"`
		TotalExpenses float64 `json:"total_expenses"`
	} `json:"static_model_parameters"`
}

type DataToVisualize struct {
	//images
	EncodedStaticImage  string `json:"encoded_static_image" bson:"encoded_static_image"`
	EncodedDynamicImage string `json:"encoded_dynamic_image" bson:"encoded_dynamic_image"`
	// static
	StaticTotalTime     string  `json:"static_total_time" bson:"static_total_time"`
	StaticTotalExpenses float64 `json:"total_expenses" bson:"total_expenses"`
	StaticCouriersCount int     `json:"total_couriers_count" bson:"total_couriers_count"`
	// dynamic
	DynamicTotalTime            string  `json:"dynamic_total_time" bson:"dynamic_total_time"`
	DynamicTotalExpenses        float64 `json:"dynamic_total_expense" bson:"dynamic_total_expense"`
	DynamicOptimalCouriersCount int     `json:"optimal_couriers_count" bson:"optimal_couriers_count"`
}

//type Geometry struct {
//	Type        string    `json:"type" bson:"type"`
//	Coordinates [][][]int `json:"coordinates" bson:"coordinates"`
//}
//
//type Feature struct {
//	Type     string   `json:"type" bson:"type"`
//	Geometry `json:"geometry" bson:"geometry"`
//}
//
//type OutputData struct {
//	UserId                 string    `json:"user_id" bson:"user_id"`
//	RequestId              string    `json:"request_id" bson:"request_id"`
//	ModelType              string    `json:"model_type" bson:"model_type"`
//	CreatedAt              time.Time `json:"created_at" bson:"created_at"`
//	CalculatedStaticValues struct {
//		StaticFuelCost  string `json:"static_fuel_cost" bson:"static_fuel_cost"`
//		StaticTimeSpent string `json:"static_time_spent" bson:"static_time_spent"`
//	} `json:"calculated_static_values" bson:"calculated_static_values"`
//	CalculatedDynamicValues struct {
//		DynamicFuelCost  string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
//		DynamicTimeSpent string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
//	} `json:"calculated_dynamic_values" bson:"calculated_dynamic_values"`
//	Data struct {
//		Type     string    `json:"type" bson:"type"`
//		Features []Feature `json:"features" bson:"features"`
//	} `json:"data" bson:"data"`
//}

type RequestIDWithTimestamp struct {
	RequestID string    `json:"request_id" bson:"request_id"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}
