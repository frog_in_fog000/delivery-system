package handlers

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	redis2 "github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage/redis"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"go.uber.org/zap"
)

func (h *visualizerHandler) SaveData(w http.ResponseWriter, r *http.Request) {
	userIdHeader := r.Header.Get("-x-user-id")
	if userIdHeader == "" {
		h.log.Logger.Warn("userIdHeader is empty", zap.String("userIdHeader", userIdHeader))
	}
	userId := strings.TrimPrefix(userIdHeader, "user_id=")

	requestId := r.Header.Get("-x-request-id")
	if requestId == "" {
		h.log.Logger.Warn("requestIdHeader is empty", zap.String("userIdHeader", userIdHeader))
	}

	staticKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "static")
	dynamicKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "dynamic")

	var outputStaticData models.GJSStaticOutput
	var outputDynamicData models.GJSDynamicOutput

	staticData, err := redis.RedisClient.Get(context.Background(), staticKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputStaticDataFromMongoDB, err := h.visualizerService.GetStaticDataByRequestId(requestId, "static")
			if err != nil {
				h.log.Logger.Error("error getting static data from mongodb", zap.Error(err))
				return
			}
			outputStaticData = *outputStaticDataFromMongoDB
			h.log.Logger.Info("static data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting static data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(staticData), &outputStaticData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}
	dynamicData, err := redis.RedisClient.Get(context.Background(), dynamicKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputDynamicDataFromMongoDB, err := h.visualizerService.GetDynamicDataByRequestId(requestId, "dynamic")
			if err != nil {
				h.log.Logger.Error("error getting dynamic data from mongodb", zap.Error(err))
				return
			}
			outputDynamicData = *outputDynamicDataFromMongoDB
			h.log.Logger.Info("dynamic data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting dynamic data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(dynamicData), &outputDynamicData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}

	var resultData models.DataToVisualize
	resultData.StaticTotalTime = fmt.Sprintf("%dh %dm %ds", outputStaticData.StaticModelParameters.TotalTime.Hours, outputStaticData.StaticModelParameters.TotalTime.Minutes, outputStaticData.StaticModelParameters.TotalTime.Seconds)
	resultData.StaticTotalExpenses = outputStaticData.StaticModelParameters.TotalExpenses
	resultData.StaticCouriersCount = outputStaticData.StaticModelParameters.CouriersCount
	resultData.DynamicTotalTime = fmt.Sprintf("%dh %dm %ds", outputDynamicData.DynamicModelParameters.TotalTime.Hours, outputDynamicData.DynamicModelParameters.TotalTime.Minutes, outputDynamicData.DynamicModelParameters.TotalTime.Seconds)
	resultData.DynamicTotalExpenses = outputDynamicData.DynamicModelParameters.TotalExpenses
	resultData.DynamicOptimalCouriersCount = outputDynamicData.DynamicModelParameters.OptimalCouriersCount

	tempFile, err := os.CreateTemp("", "download-*.csv")
	if err != nil {
		h.log.Logger.Error("error creating temp file", zap.Error(err))
		return
	}
	defer os.Remove(tempFile.Name())
	defer tempFile.Close()

	writer := csv.NewWriter(tempFile)

	header := []string{"Static total time", "Static total expenses", "Static couriers count", "Dynamic total time", "Dynamic total expenses", "Dynamic optimal couriers count"}
	if err := writer.Write(header); err != nil {
		h.log.Logger.Error("error writing csv header", zap.Error(err))
		return
	}

	record := []string{
		resultData.StaticTotalTime,
		fmt.Sprintf("%f", resultData.StaticTotalExpenses),
		fmt.Sprintf("%d", resultData.StaticCouriersCount),
		resultData.DynamicTotalTime,
		fmt.Sprintf("%f", resultData.DynamicTotalExpenses),
		fmt.Sprintf("%d", resultData.DynamicOptimalCouriersCount),
	}

	if err := writer.Write(record); err != nil {
		h.log.Logger.Error("error writing record to csv", zap.Error(err))
		return
	}

	// Очищаем буфер записи и закрываем файл
	writer.Flush()
	if err := writer.Error(); err != nil {
		h.log.Logger.Error("error flushing writer", zap.Error(err))
		return
	}

	// Получаем информацию о файле после записи данных
	fileInfo, err := tempFile.Stat()
	if err != nil {
		h.log.Logger.Error("error getting file info", zap.Error(err))
		return
	}

	// Устанавливаем заголовки ответа
	w.Header().Set("Content-Disposition", "attachment; filename=download.csv")
	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Length", strconv.FormatInt(fileInfo.Size(), 10))

	// Перемещаем курсор в начало файла
	_, err = tempFile.Seek(0, 0)
	if err != nil {
		h.log.Logger.Error("error seeking temp file", zap.Error(err))
		return
	}

	// Отправляем содержимое файла
	_, err = io.Copy(w, tempFile)
	if err != nil {
		h.log.Logger.Error("error sending csv file", zap.Error(err))
	}
}
