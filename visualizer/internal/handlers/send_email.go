package handlers

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	redis2 "github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage/redis"
	"go.uber.org/zap"
	"gopkg.in/gomail.v2"
)

func (h *visualizerHandler) SendEmail(w http.ResponseWriter, r *http.Request) {
	userIdHeader := r.Header.Get("-x-user-id")
	if userIdHeader == "" {
		h.log.Logger.Warn("userIdHeader is empty")
	}
	userId := strings.TrimPrefix(userIdHeader, "user_id=")

	requestId := r.Header.Get("-x-request-id")
	if requestId == "" {
		h.log.Logger.Warn("requestIdHeader is empty")
	}

	staticKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "static")
	dynamicKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "dynamic")

	var outputStaticData models.GJSStaticOutput
	var outputDynamicData models.GJSDynamicOutput

	staticData, err := redis.RedisClient.Get(context.Background(), staticKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputStaticDataFromMongoDB, err := h.visualizerService.GetStaticDataByRequestId(requestId, "static")
			if err != nil {
				h.log.Logger.Error("error getting static data from mongodb", zap.Error(err))
				return
			}
			outputStaticData = *outputStaticDataFromMongoDB
			h.log.Logger.Info("static data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting static data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(staticData), &outputStaticData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}

	dynamicData, err := redis.RedisClient.Get(context.Background(), dynamicKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputDynamicDataFromMongoDB, err := h.visualizerService.GetDynamicDataByRequestId(requestId, "dynamic")
			if err != nil {
				h.log.Logger.Error("error getting dynamic data from mongodb", zap.Error(err))
				return
			}
			outputDynamicData = *outputDynamicDataFromMongoDB
			h.log.Logger.Info("dynamic data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting dynamic data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(dynamicData), &outputDynamicData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}

	var resultData models.DataToVisualize
	resultData.StaticTotalTime = fmt.Sprintf("%dh %dm %ds", outputStaticData.StaticModelParameters.TotalTime.Hours, outputStaticData.StaticModelParameters.TotalTime.Minutes, outputStaticData.StaticModelParameters.TotalTime.Seconds)
	resultData.StaticTotalExpenses = outputStaticData.StaticModelParameters.TotalExpenses
	resultData.StaticCouriersCount = outputStaticData.StaticModelParameters.CouriersCount
	resultData.DynamicTotalTime = fmt.Sprintf("%dh %dm %ds", outputDynamicData.DynamicModelParameters.TotalTime.Hours, outputDynamicData.DynamicModelParameters.TotalTime.Minutes, outputDynamicData.DynamicModelParameters.TotalTime.Seconds)
	resultData.DynamicTotalExpenses = outputDynamicData.DynamicModelParameters.TotalExpenses
	resultData.DynamicOptimalCouriersCount = outputDynamicData.DynamicModelParameters.OptimalCouriersCount

	tempFile, err := createTempCSV(resultData)
	if err != nil {
		h.log.Logger.Error("error creating temp CSV file", zap.Error(err))
		return
	}
	defer os.Remove(tempFile.Name())

	err = sendEmailWithAttachment("from@example.com", "sandbox@example.com", "CSV Data", "Please find the attached CSV file.", tempFile.Name())
	if err != nil {
		h.log.Logger.Error("error sending email", zap.Error(err))
		return
	}

	fmt.Println("Email sent successfully")

	w.WriteHeader(http.StatusOK)
}

func createTempCSV(data models.DataToVisualize) (*os.File, error) {
	tempFile, err := os.CreateTemp("", "download-*.csv")
	if err != nil {
		return nil, err
	}
	defer tempFile.Close()

	writer := csv.NewWriter(tempFile)
	defer writer.Flush()

	header := []string{"Static total time", "Static total expenses", "Static couriers count", "Dynamic total time", "Dynamic total expenses", "Dynamic optimal couriers count"}
	if err := writer.Write(header); err != nil {
		return nil, err
	}

	record := []string{
		data.StaticTotalTime,
		fmt.Sprintf("%f", data.StaticTotalExpenses),
		fmt.Sprintf("%d", data.StaticCouriersCount),
		data.DynamicTotalTime,
		fmt.Sprintf("%f", data.DynamicTotalExpenses),
		fmt.Sprintf("%d", data.DynamicOptimalCouriersCount),
	}

	if err := writer.Write(record); err != nil {
		return nil, err
	}

	return tempFile, writer.Error()
}

func sendEmailWithAttachment(from, to, subject, body, filename string) error {
	msg := gomail.NewMessage()
	msg.SetHeader("From", from)
	msg.SetHeader("To", to)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/plain", body)

	msg.Attach(filename, gomail.SetCopyFunc(func(w io.Writer) error {
		file, err := os.Open(filename)
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(w, file)
		return err
	}))

	d := gomail.NewDialer("mailhog", 1025, "", "")
	return d.DialAndSend(msg)
}
