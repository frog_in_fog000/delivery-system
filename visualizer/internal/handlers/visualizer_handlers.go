package handlers

import (
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/service"
	"net/http"
)

type VisualizerHandler interface {
	ShowData(w http.ResponseWriter, r *http.Request)
	SaveData(w http.ResponseWriter, r *http.Request)
	SendEmail(w http.ResponseWriter, r *http.Request)
	GetDataById(w http.ResponseWriter, r *http.Request)
}

type visualizerHandler struct {
	visualizerService service.VisualizerService
	log               *logger.Logger
	cfg               *config.Config
}

func NewVisualizerHandler(visualizerService service.VisualizerService, log *logger.Logger, cfg *config.Config) VisualizerHandler {
	return &visualizerHandler{visualizerService: visualizerService, log: log, cfg: cfg}
}
