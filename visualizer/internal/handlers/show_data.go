package handlers

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fogleman/gg"
	_ "github.com/redis/go-redis/v9"
	redis2 "github.com/redis/go-redis/v9"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage/redis"
	"go.uber.org/zap"
	"log"
	"net/http"
	"strings"
)

//func (h *visualizerHandler) ShowData(w http.ResponseWriter, r *http.Request) {
//	userIdHeader := r.Header.Get("-x-user-id")
//	if userIdHeader == "" {
//		h.log.Logger.Warn("userIdHeader is empty", zap.String("userIdHeader", userIdHeader))
//	}
//	userId := strings.TrimPrefix(userIdHeader, "user_id=")
//
//	requestId := r.Header.Get("-x-request-id")
//	if requestId == "" {
//		h.log.Logger.Warn("requestIdHeader is empty", zap.String("userIdHeader", userIdHeader))
//	}
//
//	staticKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "static")
//	dynamicKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "dynamic")
//
//	log.Println("Static Key:", staticKey)
//	log.Println("Dynamic Key:", dynamicKey)
//
//	var outputStaticData models.OutputData
//	var outputDynamicData models.OutputData
//
//	staticData := redis.RedisClient.Get(context.Background(), staticKey)
//	dynamicData := redis.RedisClient.Get(context.Background(), dynamicKey)
//
//	staticBytes, err := staticData.Bytes()
//	if err != nil {
//		h.log.Logger.Error("error receiving data by static key", zap.Error(err))
//		return
//	}
//
//	if err = json.Unmarshal(staticBytes, &outputStaticData); err != nil {
//		h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
//		return
//	}
//
//	dynamicBytes, err := dynamicData.Bytes()
//	if err != nil {
//		h.log.Logger.Error("error receiving data by dynamic key 2", zap.Error(err))
//		return
//	}
//
//	if err = json.Unmarshal(dynamicBytes, &outputDynamicData); err != nil {
//		h.log.Logger.Error("error unmarshalling data by static key 2", zap.Error(err))
//		return
//	}
//
//	geoStaticData, err := json.Marshal(outputStaticData.Data)
//	if err != nil {
//		h.log.Logger.Error("error marshalling static data", zap.Error(err))
//		return
//	}
//
//	geoDynamicData, err := json.Marshal(outputDynamicData.Data)
//	if err != nil {
//		h.log.Logger.Error("error marshalling dynamic data", zap.Error(err))
//		return
//	}
//
//	encodedStaticImage := drawPng(w, geoStaticData)
//	encodedDynamicImage := drawPng(w, geoDynamicData)
//
//	var resultData models.DataToVisualize
//	resultData.EncodedStaticImage = encodedStaticImage
//	resultData.EncodedDynamicImage = encodedDynamicImage
//	resultData.StaticTimeSpent = outputStaticData.CalculatedStaticValues.StaticTimeSpent
//	resultData.DynamicTimeSpent = outputDynamicData.CalculatedDynamicValues.DynamicTimeSpent
//	resultData.StaticFuelCost = outputStaticData.CalculatedStaticValues.StaticFuelCost
//	resultData.DynamicFuelCost = outputDynamicData.CalculatedDynamicValues.DynamicFuelCost
//	resultData.StaticModelName = outputStaticData.ModelType
//	resultData.DynamicModelName = outputDynamicData.ModelType
//
//	resultDataJSON, err := json.Marshal(resultData)
//	if err != nil {
//		h.log.Logger.Error("error marshalling data", zap.Error(err))
//		return
//	}
//
//	w.Header().Set("Content-Type", "application/json")
//	w.Write(resultDataJSON)
//}

func (h *visualizerHandler) ShowData(w http.ResponseWriter, r *http.Request) {
	userIdHeader := r.Header.Get("-x-user-id")
	if userIdHeader == "" {
		h.log.Logger.Warn("userIdHeader is empty", zap.String("userIdHeader", userIdHeader))
	}
	userId := strings.TrimPrefix(userIdHeader, "user_id=")

	requestId := r.Header.Get("-x-request-id")
	if requestId == "" {
		h.log.Logger.Warn("requestIdHeader is empty", zap.String("userIdHeader", userIdHeader))
	}

	staticKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "static")
	dynamicKey := fmt.Sprintf("%s:%s:%s", userId, requestId, "dynamic")

	var outputStaticData models.GJSStaticOutput
	var outputDynamicData models.GJSDynamicOutput

	staticData, err := redis.RedisClient.Get(context.Background(), staticKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputStaticDataFromMongoDB, err := h.visualizerService.GetStaticDataByRequestId(requestId, "static")
			if err != nil {
				h.log.Logger.Error("error getting static data from mongodb", zap.Error(err))
				return
			}
			outputStaticData = *outputStaticDataFromMongoDB
			h.log.Logger.Info("static data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting static data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(staticData), &outputStaticData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}
	dynamicData, err := redis.RedisClient.Get(context.Background(), dynamicKey).Result()
	if err != nil {
		if errors.Is(err, redis2.Nil) {
			outputDynamicDataFromMongoDB, err := h.visualizerService.GetDynamicDataByRequestId(requestId, "dynamic")
			if err != nil {
				h.log.Logger.Error("error getting dynamic data from mongodb", zap.Error(err))
				return
			}
			outputDynamicData = *outputDynamicDataFromMongoDB
			h.log.Logger.Info("dynamic data received from mongoDB")
		} else {
			h.log.Logger.Error("error getting dynamic data from redis", zap.Error(err))
			return
		}
	} else {
		if err = json.Unmarshal([]byte(dynamicData), &outputDynamicData); err != nil {
			h.log.Logger.Error("error unmarshalling data by static key", zap.Error(err))
			return
		}
	}

	geoStaticData, err := json.Marshal(outputStaticData.StaticModel)
	if err != nil {
		h.log.Logger.Error("error marshalling static data", zap.Error(err))
		return
	}

	geoDynamicData, err := json.Marshal(outputDynamicData.DynamicModel)
	if err != nil {
		h.log.Logger.Error("error marshalling dynamic data", zap.Error(err))
		return
	}

	encodedStaticImage := drawPng(w, geoStaticData)
	encodedDynamicImage := drawPng(w, geoDynamicData)

	var resultData models.DataToVisualize
	resultData.EncodedStaticImage = encodedStaticImage
	resultData.EncodedDynamicImage = encodedDynamicImage
	resultData.StaticTotalTime = fmt.Sprintf("%dh %dm %ds", outputStaticData.StaticModelParameters.TotalTime.Hours, outputStaticData.StaticModelParameters.TotalTime.Minutes, outputStaticData.StaticModelParameters.TotalTime.Seconds)
	resultData.StaticTotalExpenses = outputStaticData.StaticModelParameters.TotalExpenses
	resultData.StaticCouriersCount = outputStaticData.StaticModelParameters.CouriersCount
	resultData.DynamicTotalTime = fmt.Sprintf("%dh %dm %ds", outputDynamicData.DynamicModelParameters.TotalTime.Hours, outputDynamicData.DynamicModelParameters.TotalTime.Minutes, outputDynamicData.DynamicModelParameters.TotalTime.Seconds)
	resultData.DynamicTotalExpenses = outputDynamicData.DynamicModelParameters.TotalExpenses
	resultData.DynamicOptimalCouriersCount = outputDynamicData.DynamicModelParameters.OptimalCouriersCount

	resultDataJSON, err := json.Marshal(resultData)
	if err != nil {
		h.log.Logger.Error("error marshalling data", zap.Error(err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resultDataJSON)
}

type Geometry struct {
	Type        string      `json:"type"`
	Coordinates [][]float64 `json:"coordinates"`
}

type Feature struct {
	Type       string            `json:"type"`
	Geometry   Geometry          `json:"geometry"`
	Properties map[string]string `json:"properties"`
}

type FeatureCollection struct {
	Type     string    `json:"type"`
	Features []Feature `json:"features"`
}

func drawPng(w http.ResponseWriter, data []byte) string {
	var collection FeatureCollection
	if err := json.Unmarshal(data, &collection); err != nil {
		return err.Error()
	}

	const width, height = 800, 600
	dc := gg.NewContext(width, height)
	dc.SetRGB(1, 1, 1)
	dc.Clear()

	minX, minY, maxX, maxY := getBounds(collection)

	// Define padding and scaling factors
	padding := 100.0 // Увеличиваем отступ слева
	scaleX := (width - 2*padding) / (maxX - minX)
	scaleY := (height - 2*padding) / (maxY - minY)

	// Draw axes
	dc.SetRGB(0, 0, 0)
	dc.SetLineWidth(2)

	// X axis
	dc.MoveTo(padding, height-padding)
	dc.LineTo(width-padding, height-padding)
	dc.Stroke()

	// Y axis
	dc.MoveTo(padding, padding)
	dc.LineTo(padding, height-padding)
	dc.Stroke()

	// Add labels to axes
	dc.SetRGB(0, 0, 0)
	dc.SetLineWidth(1)
	dc.DrawStringAnchored(fmt.Sprintf("%.4f", minX), padding, height-padding+20, 0.5, 1.0)
	dc.DrawStringAnchored(fmt.Sprintf("%.4f", maxX), width-padding, height-padding+20, 0.5, 1.0)
	dc.DrawStringAnchored(fmt.Sprintf("%.4f", minY), padding-50, height-padding, 1.0, 0.5) // Сдвигаем метку еще левее
	dc.DrawStringAnchored(fmt.Sprintf("%.4f", maxY), padding-50, padding, 1.0, 0.5)        // Сдвигаем метку еще левее

	// Add intermediate labels
	numLabels := 10
	for i := 1; i < numLabels; i++ {
		x := padding + float64(i)*(width-2*padding)/float64(numLabels)
		y := height - padding + 10
		labelX := minX + (maxX-minX)*float64(i)/float64(numLabels)
		dc.DrawStringAnchored(fmt.Sprintf("%.4f", labelX), x, y, 0.5, 1.0)

		x = padding - 10
		y = height - padding - float64(i)*(height-2*padding)/float64(numLabels)
		labelY := minY + (maxY-minY)*float64(i)/float64(numLabels)
		dc.DrawStringAnchored(fmt.Sprintf("%.4f", labelY), x-40, y, 1.0, 0.5) // Сдвигаем метку еще левее
	}

	// Draw each feature
	for _, feature := range collection.Features {
		if len(feature.Geometry.Coordinates) == 0 {
			log.Fatalf("error getting geometry coordinates")
			return ""
		}

		if feature.Geometry.Type != "LineString" {
			continue
		}

		color := feature.Properties["color"]
		if color == "" {
			color = "black" // Цвет по умолчанию.
		}
		r, g, b, a := parseColor(color)
		dc.SetRGBA(r, g, b, a)

		for i, point := range feature.Geometry.Coordinates {
			x := padding + (point[0]-minX)*scaleX
			y := height - (padding + (point[1]-minY)*scaleY) // Invert Y axis
			if i == 0 {
				dc.MoveTo(x, y)
			} else {
				dc.LineTo(x, y)
			}
		}

		dc.Stroke()
	}

	var imageBase64 strings.Builder
	if err := dc.EncodePNG(&imageBase64); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return ""
	}
	imageData := imageBase64.String()
	encodedImage := base64.StdEncoding.EncodeToString([]byte(imageData))

	return encodedImage
}

func getBounds(collection FeatureCollection) (minX, minY, maxX, maxY float64) {
	minX, minY = 180, 90
	maxX, maxY = -180, -90

	for _, feature := range collection.Features {
		for _, point := range feature.Geometry.Coordinates {
			if point[0] < minX {
				minX = point[0]
			}
			if point[1] < minY {
				minY = point[1]
			}
			if point[0] > maxX {
				maxX = point[0]
			}
			if point[1] > maxY {
				maxY = point[1]
			}
		}
	}
	return
}

func parseColor(color string) (r, g, b, a float64) {
	// Функция для разбора цвета из строки
	// Поддерживает как имена цветов, так и HEX форматы
	if strings.HasPrefix(color, "#") {
		var ri, gi, bi, ai int
		switch len(color) {
		case 7:
			fmt.Sscanf(color, "#%02x%02x%02x", &ri, &gi, &bi)
			return float64(ri) / 255, float64(gi) / 255, float64(bi) / 255, 1
		case 9:
			fmt.Sscanf(color, "#%02x%02x%02x%02x", &ri, &gi, &bi, &ai)
			return float64(ri) / 255, float64(gi) / 255, float64(bi) / 255, float64(ai) / 255
		}
	} else {
		switch color {
		case "red":
			return 1, 0, 0, 1
		case "green":
			return 0, 1, 0, 1
		case "blue":
			return 0, 0, 1, 1
		case "black":
			return 0, 0, 0, 1
		case "white":
			return 1, 1, 1, 1
		}
	}
	return 0, 0, 0, 1 // Цвет по умолчанию - черный.
}
