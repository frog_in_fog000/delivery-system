package handlers

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"
)

func (h *visualizerHandler) GetDataById(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("-x-user-id")
	if len(userId) == 0 {
		h.log.Logger.Error("userId is empty")
		return
	}

	values, err := h.visualizerService.GetAllRequestIDsByUserId(userId)
	if err != nil {
		h.log.Logger.Error("error getting data by id", zap.Error(err))
		return
	}

	jsonValues, err := json.Marshal(values)
	if err != nil {
		h.log.Logger.Error("error marshalling data", zap.Error(err))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonValues)
}
