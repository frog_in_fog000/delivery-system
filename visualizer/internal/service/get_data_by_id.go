package service

import "gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"

func (s *visualizerService) GetAllRequestIDsByUserId(id string) ([]models.RequestIDWithTimestamp, error) {
	values, err := s.visualizerStorage.GetAllRequestIDsByUserId(id)
	if err != nil {
		return nil, err
	}

	return values, nil
}

func (s *visualizerService) GetStaticDataByRequestId(requestId string, modelType string) (*models.GJSStaticOutput, error) {
	data, err := s.visualizerStorage.GetStaticDataByRequestID(requestId, modelType)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (s *visualizerService) GetDynamicDataByRequestId(requestId string, modelType string) (*models.GJSDynamicOutput, error) {
	data, err := s.visualizerStorage.GetDynamicDataByRequestID(requestId, modelType)
	if err != nil {
		return nil, err
	}

	return data, nil
}
