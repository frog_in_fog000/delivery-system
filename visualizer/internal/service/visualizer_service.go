package service

import (
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage"
)

type VisualizerService interface {
	GetAllRequestIDsByUserId(id string) ([]models.RequestIDWithTimestamp, error)
	GetStaticDataByRequestId(requestId string, modelType string) (*models.GJSStaticOutput, error)
	GetDynamicDataByRequestId(requestId string, modelType string) (*models.GJSDynamicOutput, error)
}

type visualizerService struct {
	visualizerStorage storage.VisualizerStorage
	log               *logger.Logger
}

func NewVisualizerService(visualizerStorage storage.VisualizerStorage, log *logger.Logger) VisualizerService {
	return &visualizerService{visualizerStorage: visualizerStorage, log: log}
}
