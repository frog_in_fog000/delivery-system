package main

import (
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/handlers"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/service"
	"gitlab.com/frog_in_fog000/delivery-system/visualizer/internal/storage"
)

type Handlers struct {
	VisualizerHandlers handlers.VisualizerHandler
}

func InitHttpHandlers(cfg *config.Config, visualizerStorage storage.VisualizerStorage, log logger.Logger) *Handlers {
	// service
	dispatcherService := service.NewVisualizerService(visualizerStorage, &log)

	// handlers
	visualizerHandler := handlers.NewVisualizerHandler(dispatcherService, &log, cfg)

	return &Handlers{VisualizerHandlers: visualizerHandler}
}
