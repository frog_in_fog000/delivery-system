package main

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

func (h *Handlers) InitRoutes() http.Handler {
	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler())

	router.HandleFunc("/visualizer/show/{request_id}", h.VisualizerHandlers.ShowData).Methods(http.MethodGet)
	router.HandleFunc("/visualizer/save/{request_id}", h.VisualizerHandlers.SaveData).Methods(http.MethodGet)
	router.HandleFunc("/visualizer/email/{request_id}", h.VisualizerHandlers.SendEmail).Methods(http.MethodGet)
	router.HandleFunc("/visualizer/data", h.VisualizerHandlers.GetDataById).Methods(http.MethodGet)

	return router
}
