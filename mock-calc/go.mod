module gitlab.com/frog_in_fog000/delivery-system/mock-calc

go 1.22.1

require (
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/streadway/amqp v1.1.0 // indirect
)
