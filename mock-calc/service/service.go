package service

import (
	"fmt"
	"gitlab.com/frog_in_fog000/delivery-system/mock-calc/models"
	"math/rand"
	"time"
)

func generateRandomCoordinates(numPoints int) [][][]int {
	rand.Seed(time.Now().UnixNano())
	coordinates := make([][][]int, 1)
	coordinates[0] = make([][]int, numPoints+1) // +1 для замыкания полигона
	for i := 0; i < numPoints; i++ {
		coordinates[0][i] = []int{rand.Intn(601), rand.Intn(601)}
	}
	// замыкаем полигон, добавляя первую точку в конец
	if numPoints > 0 {
		coordinates[0][numPoints] = coordinates[0][0]
	}
	return coordinates
}

func CalcStatic(input models.GeoJSONInput) (models.OutputData, error) {
	var output models.OutputData
	output.UserId = input.UserID
	output.RequestId = input.RequestID
	output.ModelType = "static"
	output.CalculatedStaticValues.StaticFuelCost = fmt.Sprintf("$%d", rand.Intn(1000))
	output.CalculatedStaticValues.StaticTimeSpent = fmt.Sprintf("%dh", rand.Intn(12))

	output.Data.Type = input.Data.Type
	output.Data.Features = make([]models.Feature, 2)

	numPoints := len(input.Data.Features.Geometry.Coordinates[0]) - 1 // исключаем последнюю точку, т.к. это повтор первой
	for i := 0; i < 2; i++ {
		output.Data.Features[i].Type = input.Data.Features.Type
		output.Data.Features[i].Geometry.Type = input.Data.Features.Geometry.Type
		output.Data.Features[i].Geometry.Coordinates = generateRandomCoordinates(numPoints)
	}

	output.CreatedAt = time.Now()

	return output, nil
}

func CalcDynamic(input models.GeoJSONInput) (models.OutputData, error) {
	var output models.OutputData
	output.UserId = input.UserID
	output.RequestId = input.RequestID
	output.ModelType = "dynamic"
	output.CalculatedDynamicValues.DynamicFuelCost = fmt.Sprintf("$%d", rand.Intn(1000))
	output.CalculatedDynamicValues.DynamicTimeSpent = fmt.Sprintf("%dh", rand.Intn(12))

	output.Data.Type = input.Data.Type
	output.Data.Features = make([]models.Feature, 2)

	numPoints := len(input.Data.Features.Geometry.Coordinates[0]) - 1 // исключаем последнюю точку, т.к. это повтор первой
	for i := 0; i < 2; i++ {
		output.Data.Features[i].Type = input.Data.Features.Type
		output.Data.Features[i].Geometry.Type = input.Data.Features.Geometry.Type
		output.Data.Features[i].Geometry.Coordinates = generateRandomCoordinates(numPoints)
	}

	output.CreatedAt = time.Now()

	return output, nil
}
