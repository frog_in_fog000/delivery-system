package models

import "time"

type ModelData struct {
	Name string `json:"name" bson:"name"`
	Data string `json:"data" bson:"data"`
}

type GeoJSONData struct {
	UserId    string `json:"user_id" bson:"user_id"`
	RequestId string `json:"request_id" bson:"request_id"`
	Data      struct {
		Type     string `json:"type" bson:"type"`
		Features []struct {
			Type     string `json:"type" bson:"type"`
			Geometry struct {
				Type        string    `json:"type" bson:"type"`
				Coordinates [][][]int `json:"coordinates" bson:"coordinates"`
			} `json:"geometry" bson:"geometry"`
		} `json:"features" bson:"features"`
	} `json:"data" bson:"data"`
}

type GeoJSONInput struct {
	UserID    string    `json:"user_id"`
	RequestID string    `json:"request_id" bson:"request_id"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	Data      struct {
		Type     string `json:"type"`
		Features struct {
			Type     string `json:"type"`
			Geometry struct {
				Type        string    `json:"type"`
				Coordinates [][][]int `json:"coordinates"`
			} `json:"geometry"`
		} `json:"features"`
	} `json:"data"`
}

type Geometry struct {
	Type        string    `json:"type" bson:"type"`
	Coordinates [][][]int `json:"coordinates" bson:"coordinates"`
}

type Feature struct {
	Type     string   `json:"type" bson:"type"`
	Geometry Geometry `json:"geometry" bson:"geometry"`
}

type CalculatedStaticData struct {
	StaticFuelCost  string `json:"static_fuel_cost" bson:"static_fuel_cost"`
	StaticTimeSpent string `json:"static_time_spent" bson:"static_time_spent"`
}

type CalculatedDynamicData struct {
	DynamicFuelCost  string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
	DynamicTimeSpent string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
}

type OutputData struct {
	UserId                 string    `json:"user_id" bson:"user_id"`
	RequestId              string    `json:"request_id" bson:"request_id"`
	ModelType              string    `json:"model_type" bson:"model_type"`
	CreatedAt              time.Time `json:"created_at" bson:"created_at"`
	CalculatedStaticValues struct {
		StaticFuelCost  string `json:"static_fuel_cost" bson:"static_fuel_cost"`
		StaticTimeSpent string `json:"static_time_spent" bson:"static_time_spent"`
	} `json:"calculated_static_values" bson:"calculated_static_values"`
	CalculatedDynamicValues struct {
		DynamicFuelCost  string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
		DynamicTimeSpent string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
	} `json:"calculated_dynamic_values" bson:"calculated_dynamic_values"`
	Data struct {
		Type     string    `json:"type" bson:"type"`
		Features []Feature `json:"features" bson:"features"`
	} `json:"data" bson:"data"`
}
