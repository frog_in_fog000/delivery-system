package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
	"gitlab.com/frog_in_fog000/delivery-system/mock-calc/models"
	"gitlab.com/frog_in_fog000/delivery-system/mock-calc/service"
)

var (
	AmqpServerUrl = "amqp://guest:guest@rabbitmq:5672/"
)

type Response struct {
	Message string `json:"message"`
}

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/calc", ModelCalculation)

	srv := http.Server{
		Addr:    ":5000",
		Handler: router,
	}

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}

}

func ModelCalculation(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	var inputData models.GeoJSONInput

	if err = json.Unmarshal(body, &inputData); err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	var resp Response
	resp.Message = "Your data has been received by mock calculation service successfully!"

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
		return
	}

	w.Write(jsonResp)

	connectRabbitMQ, err := amqp.Dial(AmqpServerUrl)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	//defer connectRabbitMQ.Close()

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	//defer channelRabbitMQ.Close()

	_, err = channelRabbitMQ.QueueDeclare(
		"GeoJSONOutput", // queue name
		true,            // durable
		false,           // auto delete
		false,           // exclusive
		false,           // no wait
		nil,             // arguments
	)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	go func(channel *amqp.Channel) {
		outputStatic, err := service.CalcStatic(inputData)
		if err != nil {
			log.Println(err)
			return
		}

		outputStaticJSON, err := json.Marshal(outputStatic)
		if err != nil {
			log.Println(err)
			return
		}

		time.Sleep(1 * time.Second)

		StaticMessage := amqp.Publishing{
			ContentType: "application/json",
			Body:        outputStaticJSON,
		}

		if err := channel.Publish(
			"",              // exchange
			"GeoJSONOutput", // queue name
			false,           // mandatory
			false,           // immediate
			StaticMessage,   // message to publish
		); err != nil {
			log.Println(err)
			return
		}
	}(channelRabbitMQ)

	go func(channel *amqp.Channel) {
		outputDynamic, err := service.CalcDynamic(inputData)
		if err != nil {
			log.Println(err)
		}

		outputDynamicJSON, err := json.Marshal(outputDynamic)
		if err != nil {
			log.Println(err)
			return
		}

		time.Sleep(2 * time.Second)

		table := make(map[string]interface{})
		table["type"] = "static"

		DynamicMessage := amqp.Publishing{
			ContentType: "application/json",
			Body:        outputDynamicJSON,
			Headers:     table,
		}

		if err := channel.Publish(
			"",              // exchange
			"GeoJSONOutput", // queue name
			false,           // mandatory
			false,           // immediate
			DynamicMessage,  // message to publish
		); err != nil {
			log.Println(err)
			return
		}
	}(channelRabbitMQ)

}
