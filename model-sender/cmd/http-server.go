package main

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/handlers"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/service"
)

type Handlers struct {
	SenderHandler handlers.SenderHandler
}

func InitHttpHandlers(cfg *config.Config, log logger.Logger) *Handlers {
	// service
	senderService := service.NewSenderService(&log)

	// handlers
	senderHandler := handlers.NewSenderHandler(senderService, &log, cfg)

	return &Handlers{
		SenderHandler: senderHandler,
	}
}
