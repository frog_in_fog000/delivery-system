package main

import (
	"context"
	"fmt"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/logger"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Server struct {
	server *http.Server
}

func main() {
	log := logger.SetupLogger()
	// config setup
	cfg, err := config.LoadConfig(".", &log)
	if err != nil {
		log.Logger.Error("error parsing env variables", zap.Error(err))
	}

	// init http handlers
	httpHandlers := InitHttpHandlers(&cfg, log)
	// launch http server
	server := new(Server)

	go func() {
		if err = server.run(cfg.WebPort, httpHandlers.InitRoutes()); err != nil {
			log.Logger.Error("error occurred running http server", zap.Error(err))
		}
	}()

	log.Logger.Info(fmt.Sprintf("Application started on port: %s", cfg.WebPort))

	gracefulShutdown(*server, log.Logger)

	log.Logger.Info("Application stopped")
}

func (s *Server) run(port string, handler http.Handler) error {
	s.server = &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      handler,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return s.server.ListenAndServe()
}

func (s *Server) shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}

func gracefulShutdown(server Server, logger *zap.Logger) {
	q := make(chan os.Signal, 1)
	signal.Notify(q, syscall.SIGTERM, syscall.SIGINT)
	<-q

	if err := server.shutdown(context.Background()); err != nil {
		logger.Error("error occurred on server shutting down", zap.Error(err))
	}
}
