# build
FROM golang:1.22.1-alpine AS builder

WORKDIR /app

RUN apk --no-cache add bash git make

COPY ["model-sender/go.mod", "model-sender/go.sum", "./"]

RUN go mod download

COPY model-sender .

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/app cmd/*.go

# run
FROM alpine as runner

COPY --from=builder /app/bin/app /
COPY model-sender/.env /config.env

EXPOSE 8081

CMD ["/app"]