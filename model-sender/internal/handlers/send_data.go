package handlers

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/google/uuid"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/models"
	"go.uber.org/zap"
)

func (h *senderHandler) SendGeoJSONData(w http.ResponseWriter, r *http.Request) {
	userIdHeader := r.Header.Get("-x-user-id")
	if userIdHeader == "" {
		h.log.Logger.Warn("userIdHeader empty", zap.String("userIdHeader", userIdHeader))
	}
	userId := strings.TrimPrefix(userIdHeader, "user_id=")

	geoJsonDataInput, err := io.ReadAll(r.Body)
	if err != nil {
		h.log.Logger.Error("failed to read body", zap.Error(err))
		return
	}
	defer r.Body.Close()

	h.log.Logger.Info("Sender request body: " + string(geoJsonDataInput))

	var geoJsonInput models.GJSInput

	if err = json.Unmarshal(geoJsonDataInput, &geoJsonInput); err != nil {
		h.log.Logger.Error("failed to unmarshal body", zap.Error(err))
		return
	}

	geoJsonInput.RequestData.UserId = userId
	geoJsonInput.RequestData.RequestId = uuid.New().String()
	geoJsonInput.RequestData.CreatedAt = time.Now()

	geoJsonReqBody, err := json.Marshal(geoJsonInput)
	if err != nil {
		h.log.Logger.Error("failed to marshal body", zap.Error(err))
		return
	}

	req, err := http.NewRequest(http.MethodPost, "http://logistics:4003/api/optimize", bytes.NewBuffer(geoJsonReqBody))
	if err != nil {
		h.log.Logger.Error("failed to create request", zap.Error(err))
		return
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		h.log.Logger.Error("failed to send request", zap.Error(err))
		return
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		h.log.Logger.Error("failed to read response body", zap.Error(err))
		return
	}

	h.log.Logger.Info(string(respBody))

	values := url.Values{}
	values.Add("user_id", geoJsonInput.RequestData.UserId)
	values.Add("request_id", geoJsonInput.RequestData.RequestId)

	http.Redirect(w, r, "http://model-receiver:8082/receiver?"+values.Encode(), http.StatusSeeOther)
}
