package handlers

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/service"
	"net/http"
)

type SenderHandler interface {
	SendGeoJSONData(w http.ResponseWriter, r *http.Request)
}

type senderHandler struct {
	senderService service.SenderService
	log           *logger.Logger
	cfg           *config.Config
}

func NewSenderHandler(sender service.SenderService, log *logger.Logger, cfg *config.Config) SenderHandler {
	return &senderHandler{senderService: sender, log: log, cfg: cfg}
}
