package models

import "time"

type ModelData struct {
	Name string `json:"name" bson:"name"`
	Data string `json:"data" bson:"data"`
}

type GJSInput struct {
	RequestData struct {
		UserId    string    `json:"user_id" bson:"user_id"`
		RequestId string    `json:"request_id" bson:"request_id"`
		CreatedAt time.Time `json:"created_at" bson:"created_at"`
	} `json:"request_data" bson:"request_data"`
	GeoJSON struct {
		Type     string `json:"type" bson:"type"`
		Features []struct {
			Type     string `json:"type" bson:"type"`
			Geometry struct {
				Type        string    `json:"type" bson:"type"`
				Coordinates []float64 `json:"coordinates" bson:"coordinates"`
			} `json:"geometry" bson:"geometry"`
			Properties struct {
				Name string `json:"name" bson:"name"`
				Type string `json:"type" bson:"type"`
			} `json:"properties" bson:"properties"`
		} `json:"features" bson:"features"`
	} `json:"geo_json" bson:"geo_json"`
	Parameters struct {
		FuelRateMobStorage    float64 `json:"fuel_rate_mob_storage" bson:"fuel_rate_mob_storage"`
		FuelRateCourierCar    float64 `json:"fuel_rate_courier_car" bson:"fuel_rate_courier_car"`
		FuelCost              int     `json:"fuel_cost" bson:"fuel_cost"`
		MobStorageRate        int     `json:"mob_storage_rate" bson:"mob_storage_rate"`
		CourierCarRate        int     `json:"courier_car_rate" bson:"courier_car_rate"`
		DriverSalary          int     `json:"driver_salary" bson:"driver_salary"`
		MaxCountCouriers      int     `json:"max_count_couriers" bson:"max_count_couriers"`
		CourierSalary         int     `json:"courier_salary" bson:"courier_salary"`
		CourierScooterRate    int     `json:"courier_scooter_rate" bson:"courier_scooter_rate"`
		EnergyConsumption     float64 `json:"energy_consumption" bson:"energy_consumption"`
		EnergyConsumptionCost float64 `json:"energy_consumption_cost" bson:"energy_consumption_cost"`
		MaxCourierCarCapacity int     `json:"max_courier_car_capacity" bson:"max_courier_car_capacity"`
		MaxDeliveryCapacity   float64 `json:"max_delivery_capacity" bson:"max_delivery_capacity"`
		MaxTime               int     `json:"max_time" bson:"max_time"`
		OrderProcessingTime   float64 `json:"order_processing_time" bson:"order_processing_time"`
		CourierScooterSpeed   int     `json:"courier_scooter_speed" bson:"courier_scooter_speed"`
		MobStorageSpeed       int     `json:"mob_storage_speed" bson:"mob_storage_speed"`
	} `json:"parameters" bson:"request_data"`
	Orders []struct {
		ID     int     `json:"id" bson:"id"`
		Volume float64 `json:"volume" bson:"volume"`
	} `json:"orders" bson:"orders"`
}

//type GeoJSONData struct {
//	UserId    string    `json:"user_id" bson:"user_id"`
//	RequestId string    `json:"request_id" bson:"request_id"`
//	CreatedAt time.Time `json:"created_at" bson:"created_at"`
//	Data      struct {
//		Type     string `json:"type" bson:"type"`
//		Features []struct {
//			Type     string `json:"type" bson:"type"`
//			Geometry struct {
//				Type        string    `json:"type" bson:"type"`
//				Coordinates [][][]int `json:"coordinates" bson:"coordinates"`
//			} `json:"geometry" bson:"geometry"`
//		} `json:"features" bson:"features"`
//	} `json:"data" bson:"data"`
//}
//
//type GeoJSONInput struct {
//	UserID    string    `json:"user_id"`
//	RequestID string    `json:"request_id" bson:"request_id"`
//	CreatedAt time.Time `json:"created_at" bson:"created_at"`
//	Data      struct {
//		Type     string `json:"type"`
//		Features struct {
//			Type     string `json:"type"`
//			Geometry struct {
//				Type        string    `json:"type"`
//				Coordinates [][][]int `json:"coordinates"`
//			} `json:"geometry"`
//		} `json:"features"`
//	} `json:"data"`
//}
