package config

import (
	"github.com/spf13/viper"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/logger"
	"go.uber.org/zap"
)

type Config struct {
	WebPort string `mapstructure:"WEB_PORT"`
}

func LoadConfig(path string, log *logger.Logger) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		log.Logger.Error("Error reading config file", zap.Error(err))
		return
	}

	if err = viper.Unmarshal(&config); err != nil {
		log.Logger.Error("Error unmarshalling config", zap.Error(err))
		return
	}
	return
}
