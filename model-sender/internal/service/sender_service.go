package service

import (
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/model-sender/internal/models"
)

type SenderService interface {
	SendGeoJSONData(data *models.GJSInput) error
}

type senderService struct {
	log *logger.Logger
}

func NewSenderService(log *logger.Logger) SenderService {
	return &senderService{log: log}
}
