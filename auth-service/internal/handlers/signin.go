package handlers

import (
	"encoding/json"
	"errors"
	"go.uber.org/zap"
	"io"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models/dto"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/service"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/utils"
)

func (h *authHandler) SignInUser(w http.ResponseWriter, r *http.Request) {
	var payload dto.SignInInput
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, err)
		return
	}
	if err = json.Unmarshal(body, &payload); err != nil {
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}

	validationErrs := dto.ValidateStruct(payload)
	if validationErrs != nil {
		h.log.Logger.Error("validation problems", zap.Any("validationErrs", validationErrs))
		utils.RenderJSON(w, validationErrs)
		return
	}

	newUser := models.User{
		Email:        payload.Email,
		PasswordHash: payload.Password,
	}

	resultData, err := h.authService.SignInUser(&newUser, h.cfg)
	if err != nil {
		if errors.Is(err, service.ErrInvalidCredentials) {
			resp := dto.OneLineResp{
				Data: service.ErrInvalidCredentials.Error(),
			}
			h.log.Logger.Error(err.Error(), zap.Error(err))
			utils.RenderJSON(w, resp)
			return
		} else if errors.Is(err, service.ErrUserNotFound) {
			resp := dto.OneLineResp{
				Data: service.ErrUserNotFound.Error(),
			}
			h.log.Logger.Error(err.Error(), zap.Error(err))
			utils.RenderJSON(w, resp)
			return
		}
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}

	resp := dto.UserData{
		Message: "Logged in",
		UserID:  resultData["user_id"],
	}
	utils.RenderJSON(w, resp)
	return
}
