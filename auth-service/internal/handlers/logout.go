package handlers

import (
	"encoding/json"
	"go.uber.org/zap"
	"io"
	"net/http"
	"time"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models/dto"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/utils"
)

func (h *authHandler) LogoutUser(w http.ResponseWriter, r *http.Request) {
	var payload dto.LogoutInput
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, err)
		return
	}

	if err = json.Unmarshal(body, &payload); err != nil {
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, err)
		return
	}

	userId := payload.UserId

	if err = h.authService.LogoutUser(userId); err != nil {
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, err)
		return
	}

	expired := time.Now().Add(-time.Hour * 24)
	loggedOutCookie := http.Cookie{
		Name:    "logged_in",
		Value:   "false",
		Expires: expired,
	}

	http.SetCookie(w, &loggedOutCookie)
}
