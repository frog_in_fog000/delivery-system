package handlers

import (
	"encoding/json"
	"errors"
	"go.uber.org/zap"
	"io"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models/dto"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/sqlite"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/utils"
	"golang.org/x/crypto/bcrypt"
)

func (h *authHandler) SignUpUser(w http.ResponseWriter, r *http.Request) {
	var payload dto.SignUpInput

	body, err := io.ReadAll(r.Body)
	if err != nil {
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}
	if err = json.Unmarshal(body, &payload); err != nil {
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}

	validationErrs := dto.ValidateStruct(payload)
	if validationErrs != nil {
		h.log.Logger.Error("validation problems", zap.Any("validationErrs", validationErrs))
		utils.RenderJSON(w, validationErrs)
		return
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(payload.Password), bcrypt.DefaultCost)
	if err != nil {
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}

	newUser := models.User{
		ID:           uuid.New().String(),
		Name:         payload.Name,
		LastName:     payload.LastName,
		Email:        payload.Email,
		PasswordHash: string(passwordHash),
	}

	if err = h.authService.SignUpUser(&newUser); err != nil {
		if errors.Is(err, sqlite.ErrUserAlreadyExists) {
			resp := dto.OneLineResp{
				Data: "User with such email already exists!",
			}
			h.log.Logger.Error(err.Error(), zap.Error(err))
			utils.RenderJSON(w, resp)
			return
		}
		resp := dto.OneLineResp{
			Data: err.Error(),
		}
		h.log.Logger.Error(err.Error(), zap.Error(err))
		utils.RenderJSON(w, resp)
		return
	}

	resp := dto.OneLineResp{
		Data: "User signed up successfully!",
	}
	utils.RenderJSON(w, resp)
	return
}
