package handlers

import (
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/logger"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/service"
)

type AuthHandler interface {
	SignUpUser(w http.ResponseWriter, r *http.Request)
	SignInUser(w http.ResponseWriter, r *http.Request)
	LogoutUser(w http.ResponseWriter, r *http.Request)
	TokenPair(w http.ResponseWriter, r *http.Request)
}

type authHandler struct {
	authService service.AuthUsecase
	cfg         *config.Config
	log         *logger.Logger
}

func NewAuthHandler(authService service.AuthUsecase, cfg *config.Config, logger logger.Logger) AuthHandler {
	return &authHandler{authService: authService, cfg: cfg, log: &logger}
}
