package service

import (
	"context"
	"go.uber.org/zap"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/redis"
)

func (s *authService) LogoutUser(userId string) error {
	errDelAccessToken := redis.RedisClient.Del(context.Background(), userId+accessSuffix).Err()
	if errDelAccessToken != nil {
		s.log.Logger.Error(errDelAccessToken.Error(), zap.Error(errDelAccessToken))
		return errDelAccessToken
	}
	errDelRefreshToken := redis.RedisClient.Del(context.Background(), userId+refreshSuffix).Err()
	if errDelRefreshToken != nil {
		s.log.Logger.Error(errDelRefreshToken.Error(), zap.Error(errDelRefreshToken))
		return errDelRefreshToken
	}

	return nil
}
