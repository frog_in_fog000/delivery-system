package service

import (
	"context"
	"errors"
	"go.uber.org/zap"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/sqlite"
)

func (s *authService) SignUpUser(user *models.User) error {
	if err := s.userStorage.CreateUser(context.Background(), user); err != nil {
		if errors.Is(err, sqlite.ErrUserAlreadyExists) {
			s.log.Logger.Error("User already exists", zap.Error(err))
			return sqlite.ErrUserAlreadyExists
		}
		return err
	}
	return nil
}
