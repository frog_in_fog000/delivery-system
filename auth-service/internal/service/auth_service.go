package service

import (
	"errors"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/logger"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage"
)

var (
	ErrInvalidCredentials = errors.New("invalid email or password")
	ErrUserNotFound       = errors.New("user not found")
	ErrInvalidToken       = errors.New("Invalid token")
)

const (
	accessSuffix  = ":access"
	refreshSuffix = ":refresh"
)

type AuthUsecase interface {
	SignUpUser(user *models.User) error
	SignInUser(user *models.User, cfg *config.Config) (map[string]string, error)
	RefreshAccessToken(refreshToken string, cfg *config.Config) (map[string]string, error)
	LogoutUser(userId string) error
	TokenPair(accessToken string, cfg *config.Config) (string, error)
}

type authService struct {
	userStorage storage.UserStorage
	log         *logger.Logger
}

func NewAuthService(userStorage storage.UserStorage, log logger.Logger) AuthUsecase {
	return &authService{userStorage: userStorage, log: &log}
}
