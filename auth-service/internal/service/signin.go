package service

import (
	"context"
	"database/sql"
	"errors"
	"go.uber.org/zap"
	"time"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/redis"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/pkg/tokens"
	"golang.org/x/crypto/bcrypt"
)

func (s *authService) SignInUser(newUser *models.User, cfg *config.Config) (map[string]string, error) {
	user, err := s.userStorage.GetUserByEmail(context.Background(), newUser.Email)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.log.Logger.Error("error getting user by email", zap.String("email", newUser.Email), zap.Error(err))
			return nil, ErrUserNotFound
		}
		return nil, err
	}

	if err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(newUser.PasswordHash)); err != nil {
		s.log.Logger.Error("error comparing password hash", zap.String("email", newUser.Email), zap.Error(err))
		return nil, ErrInvalidCredentials
	}

	accessTokenDetails, err := tokens.CreateToken(user.ID, cfg.AccessTokenExpiresIn, cfg.AccessTokenPrivateKey)
	if err != nil {
		s.log.Logger.Error("error creating access token", zap.String("email", newUser.Email), zap.Error(err))
		return nil, err
	}

	refreshTokenDetails, err := tokens.CreateToken(user.ID, cfg.RefreshTokenExpiresIn, cfg.RefreshTokenPrivateKey)
	if err != nil {
		s.log.Logger.Error("error creating refresh token", zap.String("email", newUser.Email), zap.Error(err))
		return nil, err
	}
	errAccess := redis.RedisClient.Set(context.TODO(), user.ID+accessSuffix, *accessTokenDetails.Token, time.Unix(*accessTokenDetails.ExpiresIn, 0).Sub(time.Now())).Err()
	if errAccess != nil {
		s.log.Logger.Error("error setting access token to Redis", zap.String("email", newUser.Email), zap.Error(errAccess))
		return nil, errAccess
	}

	errRefresh := redis.RedisClient.Set(context.TODO(), user.ID+refreshSuffix, *refreshTokenDetails.Token, time.Unix(*refreshTokenDetails.ExpiresIn, 0).Sub(time.Now())).Err()
	if errRefresh != nil {
		s.log.Logger.Error("error setting refresh token to Redis", zap.String("email", newUser.Email), zap.Error(errRefresh))
		return nil, errRefresh
	}

	resultData := make(map[string]string)
	resultData["user_id"] = user.ID
	resultData["access_token"] = *accessTokenDetails.Token
	resultData["refresh_token"] = *refreshTokenDetails.Token

	return resultData, nil

}
