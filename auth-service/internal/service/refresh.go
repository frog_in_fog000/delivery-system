package service

import (
	"context"
	"database/sql"
	"errors"
	"go.uber.org/zap"
	"time"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/redis"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/pkg/tokens"
)

func (s *authService) RefreshAccessToken(refreshToken string, cfg *config.Config) (map[string]string, error) {
	tokenClaims, err := tokens.ValidateToken(refreshToken, cfg.RefreshTokenPublicKey)
	if err != nil {
		s.log.Logger.Error("token validation failed", zap.Error(err))
		return nil, err
	}

	user, err := s.userStorage.GetUserById(context.Background(), tokenClaims.UserID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.log.Logger.Error("user not found", zap.Error(err))
			return nil, ErrUserNotFound
		}
		return nil, err
	}

	accessTokenDetails, err := tokens.CreateToken(user.ID, cfg.AccessTokenExpiresIn, cfg.AccessTokenPrivateKey)
	if err != nil {
		s.log.Logger.Error("failed to create access token", zap.Error(err))
		return nil, err
	}

	errAccess := redis.RedisClient.Set(context.TODO(), user.ID+accessSuffix, *accessTokenDetails.Token, time.Unix(*accessTokenDetails.ExpiresIn, 0).Sub(time.Now())).Err()
	if errAccess != nil {
		s.log.Logger.Error(errAccess.Error(), zap.Error(errAccess))
		return nil, errAccess
	}

	refreshTokenDetails, err := tokens.CreateToken(user.ID, cfg.RefreshTokenExpiresIn, cfg.RefreshTokenPrivateKey)
	if err != nil {
		s.log.Logger.Error("failed to create refresh token", zap.Error(err))
		return nil, err
	}

	errRefresh := redis.RedisClient.Set(context.TODO(), user.ID+refreshSuffix, *refreshTokenDetails.Token, time.Unix(*refreshTokenDetails.ExpiresIn, 0).Sub(time.Now())).Err()
	if errRefresh != nil {
		s.log.Logger.Error(errRefresh.Error(), zap.Error(errRefresh))
		return nil, errRefresh
	}

	tokenPair := make(map[string]string)
	tokenPair["access_token"] = *accessTokenDetails.Token
	tokenPair["refresh_token"] = *refreshTokenDetails.Token

	return tokenPair, nil
}
