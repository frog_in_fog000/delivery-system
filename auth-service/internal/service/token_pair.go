package service

import (
	"context"
	"errors"
	"go.uber.org/zap"

	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage/redis"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/pkg/tokens"
)

func (s *authService) TokenPair(accessToken string, cfg *config.Config) (string, error) {
	tokenDetails, err := tokens.ValidateToken(accessToken, cfg.AccessTokenPublicKey)
	if err != nil {
		if errors.Is(err, tokens.ErrInvalidToken) {
			refreshToken, err := redis.RedisClient.Get(context.Background(), tokenDetails.UserID+refreshSuffix).Result()
			if err != nil {
				s.log.Logger.Error("failed to get refresh token from Redis", zap.Error(err))
				return "", err
			}
			tokenPair, err := s.RefreshAccessToken(refreshToken, cfg)
			if err != nil {
				s.log.Logger.Error("failed to refresh access token", zap.Error(err))
				return "", err
			}
			refreshedAccessToken := tokenPair["access_token"]
			return refreshedAccessToken, nil
		}
		return "", err
	}

	return "allowed", nil
}
