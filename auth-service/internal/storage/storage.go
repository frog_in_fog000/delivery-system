package storage

import (
	"context"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
)

type UserStorage interface {
	CreateUser(ctx context.Context, user *models.User) error
	GetUserByEmail(ctx context.Context, email string) (*models.User, error)
	GetUserById(ctx context.Context, id string) (*models.User, error)
}
