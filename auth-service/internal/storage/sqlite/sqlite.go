package sqlite

import (
	"context"
	"database/sql"
	"errors"
	"github.com/mattn/go-sqlite3"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/models"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage"
	"go.uber.org/zap"
)

var (
	ErrUserAlreadyExists = errors.New("user already exists")
)

type sqliteDB struct {
	db  *sql.DB
	log *logger.Logger
}

func NewSQLiteStorage(storagePath string, log logger.Logger) (storage.UserStorage, error) {
	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		log.Logger.Error("failed to open database", zap.Error(err))
		return nil, err
	}

	return &sqliteDB{db: db, log: &log}, nil
}

func (s *sqliteDB) CreateUser(ctx context.Context, user *models.User) error {
	stmt, err := s.db.Prepare("INSERT INTO users(id, name, last_name, email, password_hash) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		s.log.Logger.Error("error while preparing statement", zap.Error(err))
		return err
	}

	_, err = stmt.ExecContext(ctx, user.ID, user.Name, user.LastName, user.Email, user.PasswordHash)
	if err != nil {
		var sqliteErr sqlite3.Error
		if errors.As(err, &sqliteErr) && errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
			s.log.Logger.Error("user already exists", zap.Error(err))
			return ErrUserAlreadyExists
		}
		s.log.Logger.Error("error while executing statement", zap.Error(err))
		return err
	}

	return nil
}

func (s *sqliteDB) GetUserByEmail(ctx context.Context, email string) (*models.User, error) {
	stmt, err := s.db.Prepare("SELECT id, name, last_name, email, password_hash FROM users WHERE email = ?")
	if err != nil {
		s.log.Logger.Error("error while preparing statement", zap.Error(err))
		return &models.User{}, err
	}

	row := stmt.QueryRowContext(ctx, email)
	var user models.User

	if err = row.Scan(&user.ID, &user.Name, &user.LastName, &user.Email, &user.PasswordHash); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.log.Logger.Error("user not found", zap.String("email", email))
			return &models.User{}, sql.ErrNoRows
		}
		s.log.Logger.Error("error while scanning row", zap.Error(err))
		return &models.User{}, err
	}

	return &user, nil
}

func (s *sqliteDB) GetUserById(ctx context.Context, id string) (*models.User, error) {
	stmt, err := s.db.Prepare("SELECT id, name, last_name, email, password_hash FROM users WHERE id = ?")
	if err != nil {
		s.log.Logger.Error("error while preparing statement", zap.Error(err))
		return &models.User{}, err
	}

	row := stmt.QueryRowContext(ctx, id)
	var user models.User

	if err = row.Scan(&user.ID, &user.Name, &user.LastName, &user.Email, &user.PasswordHash); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.log.Logger.Error("user not found", zap.String("id", id))
			return &models.User{}, sql.ErrNoRows
		}
		s.log.Logger.Error("error while scanning row", zap.Error(err))
		return &models.User{}, err
	}

	return &user, nil
}
