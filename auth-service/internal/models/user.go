package models

type User struct {
	ID           string
	Name         string
	LastName     string
	Email        string
	PasswordHash string
}
