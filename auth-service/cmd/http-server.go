package main

import (
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/config"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/handlers"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/service"
	"gitlab.com/frog_in_fog000/delivery-system/auth-service/internal/storage"
)

type Handlers struct {
	AuthHandler handlers.AuthHandler
}

func InitHttpHandlers(cfg *config.Config, userStorage storage.UserStorage, log logger.Logger) *Handlers {
	// service
	authService := service.NewAuthService(userStorage, log)

	// handlers
	authHandlers := handlers.NewAuthHandler(authService, cfg, log)

	return &Handlers{AuthHandler: authHandlers}
}
