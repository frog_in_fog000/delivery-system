package service

import (
	"bytes"
	"encoding/json"
	"go.uber.org/zap"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

func (s *gatewayService) RegisterUser(reqBody dto.RegistrationReqBody) (*http.Response, error) {
	jsonReqBody, err := json.Marshal(reqBody)
	if err != nil {
		s.log.Logger.Error("marshal request body failed", zap.Error(err))
		return nil, err
	}

	resp, err := http.Post("http://auth-service:4000/api/v0/register", "application/json", bytes.NewBuffer(jsonReqBody))
	if err != nil {
		s.log.Logger.Error("register user failed", zap.Error(err))
		return nil, err
	}

	return resp, nil
}
