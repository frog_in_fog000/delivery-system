package service

import (
	"context"
	"go.uber.org/zap"
	"net/http"
	"strings"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/redis"
)

func (s *gatewayService) AuthenticateUser(userId string) (*http.Response, error) {
	accessToken := ""
	var err error

	if userId != "" {
		accessToken, err = redis.RedisClient.Get(context.Background(), userId+":access").Result()
		if err != nil {
			s.log.Logger.Error("error getting access token from redis", zap.Error(err))
			return nil, err
		}

		accessToken = strings.TrimSuffix(accessToken, ":access")
	}

	req, err := http.NewRequest(http.MethodGet, "http://auth-service:4000/api/v0/tokens", nil)
	if err != nil {
		s.log.Logger.Error("error creating http request", zap.Error(err))
		return nil, err
	}
	if len(accessToken) != 0 {
		req.Header.Set("Authorization", "Bearer "+accessToken)
	}

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		s.log.Logger.Error("error sending http request", zap.Error(err))
		return nil, err
	}

	return response, nil
}
