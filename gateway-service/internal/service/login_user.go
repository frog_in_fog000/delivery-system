package service

import (
	"bytes"
	"encoding/json"
	"go.uber.org/zap"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

func (s *gatewayService) LoginUser(reqBody dto.LoginReqBody) (*http.Response, error) {
	jsonReqBody, err := json.Marshal(reqBody)
	if err != nil {
		s.log.Logger.Error("error login user", zap.Error(err))
		return nil, err
	}

	resp, err := http.Post("http://auth-service:4000/api/v0/login", "application/json", bytes.NewBuffer(jsonReqBody))
	if err != nil {
		s.log.Logger.Error("login user failed", zap.Error(err))
		return nil, err
	}

	return resp, nil
}
