package service

import (
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/logger"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

type GatewayUsecase interface {
	LoginUser(reqBody dto.LoginReqBody) (*http.Response, error)
	AuthenticateUser(userId string) (*http.Response, error)
	RegisterUser(reqBody dto.RegistrationReqBody) (*http.Response, error)
}

type gatewayService struct {
	log *logger.Logger
}

func NewGatewayService(log *logger.Logger) GatewayUsecase {
	return &gatewayService{log: log}
}
