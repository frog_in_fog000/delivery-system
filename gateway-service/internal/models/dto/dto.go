package dto

import "time"

type OneLineResp struct {
	Data string `json:"data"`
}

type LoginReqBody struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type RegistrationReqBody struct {
	Name     string `json:"first_name"`
	LastName string `json:"last_name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserData struct {
	Message      string `json:"data"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	UserID       string `json:"user_id"`
}

//type ReceivedData struct {
//	EncodedStaticImage  string `json:"encoded_static_image" bson:"encoded_static_image"`
//	EncodedDynamicImage string `json:"encoded_dynamic_image" bson:"encoded_dynamic_image"`
//	StaticModel         string `json:"static_model" bson:"static_model"`
//	StaticFuelCost      string `json:"static_fuel_cost" bson:"static_fuel_cost"`
//	StaticTimeSpent     string `json:"static_time_spent" bson:"static_time_spent"`
//	DynamicModel        string `json:"dynamic_model" bson:"dynamic_model"`
//	DynamicFuelCost     string `json:"dynamic_fuel_cost" bson:"dynamic_fuel_cost"`
//	DynamicTimeSpent    string `json:"dynamic_time_spent" bson:"dynamic_time_spent"`
//}

type ReceivedData struct {
	//images
	EncodedStaticImage  string `json:"encoded_static_image" bson:"encoded_static_image"`
	EncodedDynamicImage string `json:"encoded_dynamic_image" bson:"encoded_dynamic_image"`
	// static
	StaticTotalTime     string  `json:"static_total_time" bson:"static_total_time"`
	StaticTotalExpenses float64 `json:"total_expenses" bson:"total_expenses"`
	StaticCouriersCount int     `json:"total_couriers_count" bson:"total_couriers_count"`
	// dynamic
	DynamicTotalTime            string  `json:"dynamic_total_time" bson:"dynamic_total_time"`
	DynamicTotalExpenses        float64 `json:"dynamic_total_expense" bson:"dynamic_total_expense"`
	DynamicOptimalCouriersCount int     `json:"optimal_couriers_count" bson:"optimal_couriers_count"`
}

type Response struct {
	VisualizationURL string `json:"visualization_url" bson:"visualization_url"`
	DownloadCSVURL   string `json:"download_csv_url" bson:"download_csv_url"`
	SendEmailCSVURL  string `json:"send_email_csv_url" bson:"send_email_csv_url"`
}

type RequestIDWithTimestamp struct {
	RequestID string    `json:"request_id" bson:"request_id"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
}
