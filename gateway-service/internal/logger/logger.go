package logger

import (
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"os"
)

type Logger struct {
	Logger *zap.Logger
}

func SetupLogger() Logger {
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	zapLogger := zap.New(core, zap.AddCaller())
	log := Logger{Logger: zapLogger.With(zap.String("service_name", "gateway-service")).With(zap.String("environment", "gateway"))}
	return log
}
