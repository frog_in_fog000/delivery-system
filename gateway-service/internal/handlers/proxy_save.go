package handlers

import (
	"go.uber.org/zap"
	"io"
	"net/http"
	"strings"
)

func (h *gatewayHandler) ProxySaveData(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		targetURL := target + r.URL.Path

		req, err := http.NewRequest(http.MethodGet, targetURL, nil)
		if err != nil {
			h.log.Logger.Error("failed to create request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		userId, err := r.Cookie("user_id")
		if err != nil {
			h.log.Logger.Error("failed to get cookie from request", zap.Error(err))
			return
		}

		requestId := strings.TrimPrefix(r.URL.Path, "/visualizer/save/")

		req.Header.Add("-x-user-id", userId.Value)
		req.Header.Add("-x-request-id", requestId)

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			h.log.Logger.Error("failed to send request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		defer resp.Body.Close()

		// Копируем заголовки
		for name, values := range resp.Header {
			for _, value := range values {
				w.Header().Add(name, value)
			}
		}

		// Копируем статус
		w.WriteHeader(resp.StatusCode)

		// Копируем тело ответа
		_, err = io.Copy(w, resp.Body)
		if err != nil {
			h.log.Logger.Error("failed to write response", zap.Error(err))
		}

	}
}
