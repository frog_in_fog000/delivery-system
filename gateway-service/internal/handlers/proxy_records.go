package handlers

import (
	"encoding/json"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
	"go.uber.org/zap"
	"html/template"
	"io"
	"net/http"
)

func (h *gatewayHandler) ProxyGetDataById(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		targetURL := target + r.URL.Path

		req, err := http.NewRequest(http.MethodGet, targetURL, nil)
		if err != nil {
			h.log.Logger.Error("failed to create request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}

		userId, err := r.Cookie("user_id")
		if err != nil {
			h.log.Logger.Error("failed to get cookie from request", zap.Error(err))
			return
		}

		req.Header.Add("-x-user-id", userId.Value)

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			h.log.Logger.Error("failed to send request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			h.log.Logger.Error("failed to read response body", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}

		var audit []dto.RequestIDWithTimestamp

		if err = json.Unmarshal(body, &audit); err != nil {
			h.log.Logger.Error("failed to unmarshal response body", zap.Error(err))
			return
		}

		tmpl, err := template.ParseFS(h.tplFolder, "static/audit.html")
		if err != nil {
			h.log.Logger.Error("failed to parse template", zap.Error(err))
		}
		if err = tmpl.Execute(w, audit); err != nil {
			h.log.Logger.Error("failed to execute login page", zap.Error(err))
		}
	}
}
