package handlers

import (
	"go.uber.org/zap"
	"net/http"
	"strings"
)

func (h *gatewayHandler) ProxySendEmail(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		targetURL := target + r.URL.Path

		req, err := http.NewRequest(http.MethodGet, targetURL, nil)
		if err != nil {
			h.log.Logger.Error("failed to create request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		userId, err := r.Cookie("user_id")
		if err != nil {
			h.log.Logger.Error("failed to get cookie from request", zap.Error(err))
			return
		}

		requestId := strings.TrimPrefix(r.URL.Path, "/visualizer/email/")

		req.Header.Add("-x-user-id", userId.Value)
		req.Header.Add("-x-request-id", requestId)

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			h.log.Logger.Error("failed to send request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode == http.StatusOK {
			// временное решения, для разработки
			http.Redirect(w, r, "http://localhost:8025", http.StatusSeeOther)
		} else {
			h.log.Logger.Error("something unexpected happened", zap.Int("status_code", resp.StatusCode))
			return
		}

		//body, err := io.ReadAll(resp.Body)
		//if err != nil {
		//	h.log.Logger.Error("failed to read response body", zap.Error(err))
		//	http.Error(w, err.Error(), http.StatusBadGateway)
		//	return
		//}
		//
		//var receivedData dto.OneLineResp
		//if err = json.Unmarshal(body, &receivedData); err != nil {
		//	h.log.Logger.Error("failed to unmarshal response body", zap.Error(err))
		//	return
		//}
	}
}
