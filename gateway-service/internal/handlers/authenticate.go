package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/url"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

func (h *gatewayHandler) Authenticate(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var result dto.OneLineResp

		userIdCookie, err := r.Cookie("user_id")
		var userId string
		if err == nil {
			userId = userIdCookie.Value
		} else if errors.Is(err, http.ErrNoCookie) {
			userId = ""
		} else {
			h.log.Logger.Error("failed to get user id", zap.Error(err))
			return
		}

		h.log.Logger.Info(fmt.Sprintf("%s:%s", r.URL.Path, userId))

		response, err := h.gatewayService.AuthenticateUser(userId)
		if err != nil {
			h.log.Logger.Error("failed to authenticate user", zap.Error(err))
			return
		}

		defer response.Body.Close()

		body, err := io.ReadAll(response.Body)
		if err != nil {
			h.log.Logger.Error("failed to read response body", zap.Error(err))
			return
		}

		if err = json.Unmarshal(body, &result); err != nil {
			h.log.Logger.Error("failed to unmarshal response body", zap.Error(err))
			return
		}

		if result.Data == "empty access token" {
			h.log.Logger.Warn("empty access token detected", zap.String("userId", userId))
			http.Redirect(w, r, fmt.Sprintf("/login?originalPath=%s", url.QueryEscape(r.URL.Path)), http.StatusSeeOther)
			return
		}

		next(w, r)
	}
}
