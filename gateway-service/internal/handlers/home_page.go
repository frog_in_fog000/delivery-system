package handlers

import (
	"go.uber.org/zap"
	"html/template"
	"net/http"
)

func (h *gatewayHandler) HomePage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFS(h.tplFolder, "static/home.html")
	if err != nil {
		h.log.Logger.Error("failed to parse home template", zap.Error(err))
		return
	}
	if err = tmpl.Execute(w, nil); err != nil {
		h.log.Logger.Error("failed to execute home page", zap.Error(err))
		return
	}
}
