package handlers

import (
	"go.uber.org/zap"
	"html/template"
	"net/http"
)

func (h *gatewayHandler) LoginPage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFS(h.tplFolder, "static/login.html")
	if err != nil {
		h.log.Logger.Error("failed to parse template", zap.Error(err))
	}
	if err = tmpl.Execute(w, struct {
		OriginalPath string
	}{
		OriginalPath: r.URL.Query().Get("originalPath"),
	}); err != nil {
		h.log.Logger.Error("failed to execute login page", zap.Error(err))
	}
}
