package handlers

import (
	"encoding/json"
	"go.uber.org/zap"
	"io"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

func (h *gatewayHandler) LoginHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		h.log.Logger.Error("failed to parse form", zap.Error(err))
		return
	}

	email := r.FormValue("email")
	password := r.FormValue("password")
	originalPath := r.FormValue("originalPath")

	var loginRespBody dto.UserData
	reqBody := dto.LoginReqBody{
		Email:    email,
		Password: password,
	}

	resp, err := h.gatewayService.LoginUser(reqBody)
	if err != nil {
		h.log.Logger.Error("failed to login", zap.Error(err))
		return
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		h.log.Logger.Error("failed to read response body", zap.Error(err))
		return
	}

	if err = json.Unmarshal(respBody, &loginRespBody); err != nil {
		h.log.Logger.Error("failed to unmarshal response body", zap.Error(err))
		return
	}

	if loginRespBody.Message == "user not found" {
		h.log.Logger.Warn("User not found", zap.String("email", email))
		h.log.Logger.Info("Redirect to registration page", zap.String("email", email))
		http.Redirect(w, r, "/registration", http.StatusSeeOther)
		return
	} else if loginRespBody.Message == "invalid email or password" {
		h.log.Logger.Warn("Invalid email or password", zap.String("email", email))
		h.log.Logger.Info("Redirect to login page", zap.String("email", email))
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	} else if loginRespBody.Message == "Logged in" {
		h.log.Logger.Info("Logged in successfully", zap.String("email", email))
		userIdCookie := &http.Cookie{
			Name:   "user_id",
			Value:  loginRespBody.UserID,
			MaxAge: h.cfg.AccessTokenMaxAge * 60,
		}
		// Проверяем наличие других cookie с именем "user_id"
		for _, cookie := range r.Cookies() {
			if cookie.Name == "user_id" {
				// Удаляем найденный cookie
				deleteCookie := &http.Cookie{
					Name:   cookie.Name,
					Value:  "",
					MaxAge: -1, // Устанавливаем MaxAge в отрицательное значение для удаления cookie
				}
				http.SetCookie(w, deleteCookie)
			}
		}

		// Устанавливаем новый cookie "user_id"
		http.SetCookie(w, userIdCookie)

		h.log.Logger.Info("Source path" + originalPath)

		http.Redirect(w, r, originalPath, http.StatusSeeOther)
	}
}
