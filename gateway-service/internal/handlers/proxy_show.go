package handlers

import (
	"encoding/json"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
	"go.uber.org/zap"
	"html/template"
	"io"
	"net/http"
	"strings"
)

func (h *gatewayHandler) ProxyShowData(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		targetURL := target + r.URL.Path

		req, err := http.NewRequest(http.MethodGet, targetURL, nil)
		if err != nil {
			h.log.Logger.Error("failed to create request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		userId, err := r.Cookie("user_id")
		if err != nil {
			h.log.Logger.Error("failed to get cookie from request", zap.Error(err))
			return
		}

		requestId := strings.TrimPrefix(r.URL.Path, "/visualizer/show/")

		req.Header.Add("-x-user-id", userId.Value)
		req.Header.Add("-x-request-id", requestId)

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			h.log.Logger.Error("failed to send request", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			h.log.Logger.Error("failed to read response body", zap.Error(err))
			http.Error(w, err.Error(), http.StatusBadGateway)
			return
		}

		var receivedData dto.ReceivedData

		h.log.Logger.Info(string(body))

		if err = json.Unmarshal(body, &receivedData); err != nil {
			h.log.Logger.Error("failed to unmarshal received data", zap.Error(err))
			return
		}

		tmpl, err := template.ParseFS(h.tplFolder, "static/output.html")
		if err != nil {
			h.log.Logger.Error("failed to parse template", zap.Error(err))
		}
		if err = tmpl.Execute(w, receivedData); err != nil {
			h.log.Logger.Error("failed to execute login page", zap.Error(err))
		}
	}
}
