package handlers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
	"go.uber.org/zap"
	"html/template"
	"io"
	"net/http"
)

func (h *gatewayHandler) ProxySenderRaw(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//targetURL := target + r.URL.Path

		if r.Method == http.MethodGet {
			tmpl, err := template.ParseFS(h.tplFolder, "static/geojson_input_raw.html")
			if err != nil {
				h.log.Logger.Error("failed to parse template", zap.Error(err))
			}
			if err = tmpl.Execute(w, nil); err != nil {
				h.log.Logger.Error("failed to execute login page", zap.Error(err))
			}
		} else if r.Method == http.MethodPost {
			frontEndBody, err := io.ReadAll(r.Body)
			if err != nil {
				h.log.Logger.Error("failed to read body from front end", zap.Error(err))
				return
			}

			req, err := http.NewRequest(http.MethodPost, target+"/sender", bytes.NewBuffer(frontEndBody))
			if err != nil {
				h.log.Logger.Error("failed to create request", zap.Error(err))
				return
			}
			userId, _ := r.Cookie("user_id")

			req.Header.Add("-x-user-id", userId.Value)

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				h.log.Logger.Error("failed to send request", zap.Error(err))
				return
			}
			defer resp.Body.Close()

			body, err := io.ReadAll(resp.Body)
			if err != nil {
				h.log.Logger.Error("failed to read response body", zap.Error(err))
				return
			}

			var response dto.Response

			if err = json.Unmarshal(body, &response); err != nil {
				h.log.Logger.Error("failed to unmarshal response", zap.Error(err))
				return
			}

			tmplResp, err := template.ParseFS(h.tplFolder, "static/response.html")
			if err != nil {
				h.log.Logger.Error("failed to parse template", zap.Error(err))
			}
			if err = tmplResp.Execute(w, response); err != nil {
				h.log.Logger.Error("failed to execute login page", zap.Error(err))
			}
		}
	}
}
