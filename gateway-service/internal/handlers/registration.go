package handlers

import (
	"encoding/json"
	"go.uber.org/zap"
	"io"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
)

func (h *gatewayHandler) RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		h.log.Logger.Error("failed to parse form", zap.Error(err))
	}

	var regResp dto.OneLineResp

	firstName := r.FormValue("first_name")
	lastName := r.FormValue("last_name")
	email := r.FormValue("email")
	password := r.FormValue("password")

	reqBody := dto.RegistrationReqBody{
		Name:     firstName,
		LastName: lastName,
		Email:    email,
		Password: password,
	}

	resp, err := h.gatewayService.RegisterUser(reqBody)
	if err != nil {
		h.log.Logger.Error("failed to register user", zap.Error(err))
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		h.log.Logger.Error("failed to read response body", zap.Error(err))
		return
	}

	if err = json.Unmarshal(body, &regResp); err != nil {
		h.log.Logger.Error("failed to unmarshal response body", zap.Error(err))
		return
	}

	switch regResp.Data {
	case "User with such email already exists!":
		h.log.Logger.Warn("User with such email already exists!", zap.String("email", email))
		h.log.Logger.Info("Redirect to registration page")
		http.Redirect(w, r, "/registration", http.StatusSeeOther)
		return
	case "User signed up successfully!":
		h.log.Logger.Warn("User signed up successfully", zap.String("email", email))
		h.log.Logger.Info("Redirect to login page")
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	default:
		h.log.Logger.Error("something went wrong", zap.String("email", email))
		return
	}

}
