package handlers

import (
	"embed"
	"github.com/gorilla/sessions"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/logger"
	"net/http"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/config"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/service"
)

type GatewayHandler interface {
	HomePage(w http.ResponseWriter, r *http.Request)
	HomeHandler(w http.ResponseWriter, r *http.Request)
	LoginPage(w http.ResponseWriter, r *http.Request)
	RegistrationPage(w http.ResponseWriter, r *http.Request)
	LoginHandler(w http.ResponseWriter, r *http.Request)
	LogoutHandler(w http.ResponseWriter, r *http.Request)
	RegistrationHandler(w http.ResponseWriter, r *http.Request)
	Authenticate(http.HandlerFunc) http.HandlerFunc
	ProxySenderRaw(target string) http.HandlerFunc
	ProxySenderFile(target string) http.HandlerFunc
	ProxyShowData(target string) http.HandlerFunc
	ProxySaveData(target string) http.HandlerFunc
	ProxySendEmail(target string) http.HandlerFunc
	ProxyGetDataById(target string) http.HandlerFunc
	ProxyDataItemPage(w http.ResponseWriter, r *http.Request)
}

type gatewayHandler struct {
	gatewayService service.GatewayUsecase
	cfg            *config.Config
	log            *logger.Logger
	tplFolder      embed.FS
	sessionStore   *sessions.CookieStore
}

func NewGatewayHandler(gatewayService service.GatewayUsecase, cfg *config.Config, log *logger.Logger, tplFolder embed.FS, sessionStore *sessions.CookieStore) GatewayHandler {
	return &gatewayHandler{gatewayService: gatewayService, cfg: cfg, log: log, tplFolder: tplFolder, sessionStore: sessionStore}
}
