package handlers

import (
	"errors"
	"go.uber.org/zap"
	"net/http"
)

func (h *gatewayHandler) LogoutHandler(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie("user_id")
	if err != nil {
		if errors.Is(err, http.ErrNoCookie) {
			h.log.Logger.Error("logout cookie err", zap.Error(err))
			return
		}
	}

	deleteCookie := &http.Cookie{
		Name:   "user_id",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, deleteCookie)

	http.Redirect(w, r, "/", http.StatusFound)
}
