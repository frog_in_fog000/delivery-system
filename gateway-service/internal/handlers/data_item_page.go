package handlers

import (
	"fmt"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
	"go.uber.org/zap"
	"html/template"
	"net/http"
	"strings"
)

func (h *gatewayHandler) ProxyDataItemPage(w http.ResponseWriter, r *http.Request) {
	requestId := strings.TrimPrefix(r.URL.Path, "/visualizer/data/item/")

	var body dto.Response
	body.VisualizationURL = fmt.Sprintf("http://localhost:8080/visualizer/show/%s", requestId)
	body.DownloadCSVURL = fmt.Sprintf("http://localhost:8080/visualizer/save/%s", requestId)
	body.SendEmailCSVURL = fmt.Sprintf("http://localhost:8080/visualizer/email/%s", requestId)

	tmplResp, err := template.ParseFS(h.tplFolder, "static/audit_inside.html")
	if err != nil {
		h.log.Logger.Error("failed to parse template", zap.Error(err))
	}
	if err = tmplResp.Execute(w, body); err != nil {
		h.log.Logger.Error("failed to execute login page", zap.Error(err))
	}
}
