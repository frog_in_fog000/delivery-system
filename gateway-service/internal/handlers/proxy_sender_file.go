package handlers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/models/dto"
	"go.uber.org/zap"
	"html/template"
	"io"
	"net/http"
)

func (h *gatewayHandler) ProxySenderFile(target string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			tmpl, err := template.ParseFS(h.tplFolder, "static/geojson_input_file.html")
			if err != nil {
				h.log.Logger.Error("failed to parse template", zap.Error(err))
			}
			if err = tmpl.Execute(w, nil); err != nil {
				h.log.Logger.Error("failed to execute login page", zap.Error(err))
			}
		} else if r.Method == http.MethodPost {

			file, _, err := r.FormFile("file")
			if err != nil {
				h.log.Logger.Error("failed to get file", zap.Error(err))
				return
			}
			defer file.Close()

			body, err := io.ReadAll(file)
			if err != nil {
				h.log.Logger.Error("failed to read body", zap.Error(err))
				return
			}

			req, err := http.NewRequest(http.MethodPost, target+"/sender", bytes.NewBuffer(body))
			if err != nil {
				h.log.Logger.Error("failed to create request", zap.Error(err))
				return
			}
			userId, _ := r.Cookie("user_id")

			req.Header.Set("-x-user-id", userId.Value)

			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				h.log.Logger.Error("failed to send request", zap.Error(err))
				return
			}
			defer resp.Body.Close()

			respBody, err := io.ReadAll(resp.Body)
			if err != nil {
				h.log.Logger.Error("failed to read response body", zap.Error(err))
				return
			}

			var response dto.Response

			if err = json.Unmarshal(respBody, &response); err != nil {
				h.log.Logger.Error("failed to unmarshal response", zap.Error(err))
				return
			}

			tmplResp, err := template.ParseFS(h.tplFolder, "static/response.html")
			if err != nil {
				h.log.Logger.Error("failed to parse template", zap.Error(err))
			}
			if err = tmplResp.Execute(w, response); err != nil {
				h.log.Logger.Error("failed to execute login page", zap.Error(err))
			}
		}
	}
}
