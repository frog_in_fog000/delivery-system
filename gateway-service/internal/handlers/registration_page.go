package handlers

import (
	"go.uber.org/zap"
	"html/template"
	"net/http"
)

func (h *gatewayHandler) RegistrationPage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFS(h.tplFolder, "static/registration.html")
	if err != nil {
		h.log.Logger.Error("failed to parse template", zap.Error(err))
	}
	if err = tmpl.Execute(w, nil); err != nil {
		h.log.Logger.Error("failed to execute login page", zap.Error(err))
	}
}
