package main

import (
	"embed"
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/logger"
	"go.uber.org/zap"

	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/config"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/redis"
)

//go:embed static
var tplFolder embed.FS

func main() {
	var sessionStore = sessions.NewCookieStore([]byte("secret")) // Создание хранилища сессий с секретным ключом

	log := logger.SetupLogger()

	cfg, err := config.LoadConfig(".", &log)
	if err != nil {
		log.Logger.Error("Error parsing env variables", zap.Error(err))
	}

	if err = redis.NewRedisConnection(&cfg, &log); err != nil {
		log.Logger.Error("error connecting to redis", zap.Error(err))
	}

	httpHandlers := InitHttpHandlers(&cfg, &log, tplFolder, sessionStore)
	router := httpHandlers.InitRoutes()

	log.Logger.Info("Gateway service is running on port", zap.String("port", cfg.WebPort))

	http.ListenAndServe(fmt.Sprintf(":%s", cfg.WebPort), router)
}
