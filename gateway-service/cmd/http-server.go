package main

import (
	"embed"
	"github.com/gorilla/sessions"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/config"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/handlers"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/logger"
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/service"
)

type Handlers struct {
	GatewayHandler handlers.GatewayHandler
}

func InitHttpHandlers(cfg *config.Config, log *logger.Logger, tplFolder embed.FS, sessionStore *sessions.CookieStore) *Handlers {
	// service
	gatewayService := service.NewGatewayService(log)

	// handlers
	gatewayHandler := handlers.NewGatewayHandler(gatewayService, cfg, log, tplFolder, sessionStore)

	return &Handlers{GatewayHandler: gatewayHandler}
}
