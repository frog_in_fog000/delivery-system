package main

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"

	"github.com/gorilla/mux"
)

func (h *Handlers) InitRoutes() http.Handler {
	router := mux.NewRouter()

	router.Handle("/metrics", promhttp.Handler())

	router.HandleFunc("/", h.GatewayHandler.HomePage).Methods(http.MethodGet)
	router.HandleFunc("/", h.GatewayHandler.HomeHandler).Methods(http.MethodPost)
	router.HandleFunc("/login", h.GatewayHandler.LoginPage).Methods(http.MethodGet)
	router.HandleFunc("/login", h.GatewayHandler.LoginHandler).Methods(http.MethodPost)
	router.HandleFunc("/logout", h.GatewayHandler.LogoutHandler).Methods(http.MethodGet)
	router.HandleFunc("/registration", h.GatewayHandler.RegistrationPage).Methods(http.MethodGet)
	router.HandleFunc("/registration", h.GatewayHandler.RegistrationHandler).Methods(http.MethodPost)
	router.HandleFunc("/sender/raw", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxySenderRaw("http://model-sender:8081")))
	router.HandleFunc("/sender/file", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxySenderFile("http://model-sender:8081")))
	router.HandleFunc("/visualizer/show/{request_id}", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxyShowData("http://visualizer:8083")))
	router.HandleFunc("/visualizer/save/{request_id}", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxySaveData("http://visualizer:8083")))
	router.HandleFunc("/visualizer/email/{request_id}", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxySendEmail("http://visualizer:8083")))
	router.HandleFunc("/visualizer/data", h.GatewayHandler.Authenticate(h.GatewayHandler.ProxyGetDataById("http://visualizer:8083")))
	router.HandleFunc("/visualizer/data/item/{request_id}", h.GatewayHandler.ProxyDataItemPage)

	return router
}
