package config

import (
	"gitlab.com/frog_in_fog000/delivery-system/gateway-service/internal/logger"
	"go.uber.org/zap"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	WebPort  string `mapstructure:"WEB_PORT"`
	RedisURL string `mapstructure:"REDIS_URL"`

	AccessTokenExpiresIn  time.Duration `mapstructure:"ACCESS_TOKEN_EXPIRED_IN"`
	RefreshTokenExpiresIn time.Duration `mapstructure:"REFRESH_TOKEN_EXPIRED_IN"`
	AccessTokenMaxAge     int           `mapstructure:"ACCESS_TOKEN_MAXAGE"`
	RefreshTokenMaxAge    int           `mapstructure:"REFRESH_TOKEN_MAXAGE"`
}

func LoadConfig(path string, log *logger.Logger) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		log.Logger.Error("failed to read config", zap.Error(err))
		return
	}

	if err = viper.Unmarshal(&config); err != nil {
		log.Logger.Error("failed to unmarshal config", zap.Error(err))
	}
	return
}
